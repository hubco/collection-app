package com.cracklane.collection.app.common.extensions

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target

fun ImageView.load(url: Any?, placeholder: Int) {
    Glide.with(context)
        .setDefaultRequestOptions(RequestOptions().placeholder(placeholder))
        .load(url)
        .thumbnail(0.05f)
        .into(this)
}

fun ImageView.load(url: Any?, placeholder: Drawable? = null) {
    Glide.with(context)
        .setDefaultRequestOptions(RequestOptions().placeholder(placeholder))
        .load(url)
        .thumbnail(0.05f)
        .into(this)
}

fun ImageView.loadGif(resourceId: Int, callback: (() -> Unit)? = null) {
    Glide.with(this)
        .asGif()
        .load(resourceId)
        .listener(object : RequestListener<GifDrawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<GifDrawable>?,
                isFirstResource: Boolean
            ): Boolean {
                return false
            }

            override fun onResourceReady(
                resource: GifDrawable?,
                model: Any?,
                target: Target<GifDrawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                resource?.setLoopCount(1)
                resource?.registerAnimationCallback(object : Animatable2Compat.AnimationCallback() {
                    override fun onAnimationEnd(drawable: Drawable?) {
                        resource.clearAnimationCallbacks()
                        callback?.invoke()
                    }
                })
                return false
            }
        })
        .into(this)
}