package com.cracklane.collection.app.common.extensions

import android.app.Activity
import android.content.Context
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast

/**
 * Show hidden view
 */
fun View.showView() {
    if (!this.isShown) this.visibility = View.VISIBLE
}

/**
 * Hide View
 */
fun View.hideView() {
    if (this.isShown) this.visibility = View.GONE
}

/**
 * Show multiple views
 */
fun showViews(vararg views: View) {
    views.forEach { view -> view.showView() }
}

/**
 * Hide multiple views
 */
fun hideViews(vararg views: View) {
    views.forEach { view -> view.hideView() }
}

/**
 * Extension method to show a keyboard for View.
 */
fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.requestFocus()
    imm.showSoftInput(this, 0)
}

/**
 * Try to hide the keyboard and returns whether it worked
 * https://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
 */
fun View.hideKeyboard(): Boolean {
    try {
        val inputMethodManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

/**
 * Try to hide the keyboard and returns whether it worked
 * https://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
 */
fun Activity.hideKeyboard(): Boolean {
    try {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

/**
 * Extension method to show toast for Context.
 */
fun Context.toast(text: CharSequence, duration: Int = Toast.LENGTH_LONG) =
    Toast.makeText(this, text, duration).show()

fun Context.toDp(value: Float) =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, resources.displayMetrics)

fun String.removeSpecialChars(): String {
    return Regex("[^a-zA-Z0-9]").replace(this, "")
}

fun Context.toast(message: CharSequence) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.toastInCenter(message: CharSequence) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).apply {
        setGravity(Gravity.CENTER, 0, 0)
    }.show()
}

fun Context.toastInCenter(textId: Int) {
    Toast.makeText(this, getString(textId), Toast.LENGTH_SHORT).apply {
        setGravity(Gravity.CENTER, 0, 0)
    }.show()
}

fun Context.toast(textId: Int) {
    Toast.makeText(this, getString(textId), Toast.LENGTH_SHORT).show()
}