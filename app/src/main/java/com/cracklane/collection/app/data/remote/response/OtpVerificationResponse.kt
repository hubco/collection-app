package com.cracklane.collection.app.data.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OtpVerificationResponse(
    var userName: String? = null,
    val message: String,
    @SerializedName("t_id")
    val otpId: String,
    val otpPrefix: String,
    val status: String
) : Parcelable