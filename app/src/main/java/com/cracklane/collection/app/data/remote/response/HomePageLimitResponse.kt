package com.cracklane.collection.app.data.remote.response

import com.google.gson.annotations.SerializedName

data class HomePageLimitResponse(
    val status: String,
    @SerializedName("collection_limit")
    val collectionLimit: String?,
    @SerializedName("available_limit")
    val availableLimit: String?,
    @SerializedName("other_in_hand")
    val otherPayment: String?,
    @SerializedName("cash_in_hand")
    val cashInHand: String?,
    @SerializedName("pending_approvals_collection")
    val pendingCollection: String?,
    @SerializedName("pending_branch_cash_deposit")
    val branchCashDeposit: String?,
    @SerializedName("today_collection")
    val todayCollection: String?,
    @SerializedName("today_withdrawal")
    val todayWithdrawal: String?
)
