package com.cracklane.collection.app.common

enum class CollectionType {
    SINGLE,
    OTP_DEPOSIT,
    OTP_WITHDRAWAL
}