package com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.inbuilt

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.session.ISession
import com.cracklane.collection.app.databinding.FragmentInbuiltBinding
import com.cracklane.collection.app.ui.fragment.base.BaseFragment
import com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.InvoicePrintSettingsActivity
import com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.BasePrinterFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class InbuiltFragment : BasePrinterFragment<InbuiltViewModel, FragmentInbuiltBinding>() {

    override val viewModel: InbuiltViewModel by viewModels()
    override val layoutRes: Int = R.layout.fragment_inbuilt
    lateinit var recyclerView: RecyclerView

    private var inbuiltAdapter: InbuiltAdapter? = null

    @Inject
    lateinit var session: ISession

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = dataBinding.recyclerview
        recyclerView.layoutManager = LinearLayoutManager(activity)
        inbuiltAdapter = InbuiltAdapter(ArrayList(), session) {
            (requireActivity() as InvoicePrintSettingsActivity).notifyPrinterChanged()
        }
        recyclerView.adapter = inbuiltAdapter
    }

    override fun registerObserver() {
        viewModel.inbuiltLiveList.observe(this, {
            inbuiltAdapter?.setPrinterList(it)
        })
    }

    override fun notifyPrinterChanged() {
        inbuiltAdapter?.refreshSelectedPrinter()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            InbuiltFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}