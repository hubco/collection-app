package com.cracklane.collection.app.ui.view.activity.collection

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.common.CollectionType
import com.cracklane.collection.app.common.architecture.NotifyChangeLiveData
import com.cracklane.collection.app.common.architecture.SingleLiveEvent
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.dataclass.Status
import com.cracklane.collection.app.data.remote.response.*
import com.cracklane.collection.app.domain.repository.IAccountRepository
import com.cracklane.collection.app.domain.repository.ISettingsRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OTPWithdrawViewModel @Inject constructor(
    private val settingsRepository: ISettingsRepository,
    private val accountRepository: IAccountRepository
) : BaseViewModel() {
    private var _accountList = MutableLiveData<List<String>>()
    val accountList: LiveData<List<String>> = _accountList

    private val _isAccountDetailsLoading = MutableLiveData<Boolean>()
    val isAccountDetailsLoading: LiveData<Boolean> = _isAccountDetailsLoading

    private val _otpRequestResultData = MutableLiveData<Result<CollectionSubmitOtpResponse>>()
    val otpRequestResultData: LiveData<Result<CollectionSubmitOtpResponse>>
        get() = _otpRequestResultData

    private val _verifyOtpResultData = MutableLiveData<Result<CollectionSubmitResponse>>()
    val verifyOtpResultData: LiveData<Result<CollectionSubmitResponse>>
        get() = _verifyOtpResultData

    private val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    val accountType = NotifyChangeLiveData<String> {
        notifyAccountTypeChanged()
    }

    val accountNumber = MutableLiveData<String>()
    val memberName = MutableLiveData<String>()
    val memberNumber = MutableLiveData<String>()
    val memberScheme = MutableLiveData<String>()
    val openDate = MutableLiveData<String>()
    val amountBalance = MutableLiveData<String>()
    val amountToPay = MutableLiveData<String>()
    val amount = MutableLiveData<String>()
    val remarks = MutableLiveData<String>()
    private var _slug: String = ""
    val hasAccountDetails = MutableLiveData<Boolean>()

    private var _collectionType = CollectionType.OTP_WITHDRAWAL
    val isAccountTypeRequired = MutableLiveData<Boolean>()

    private var localAccountList = mutableListOf<NameValuePair>()

    val selectedAccountCode: String
        get() = when (_collectionType) {
            CollectionType.SINGLE -> localAccountList.find { it.name == accountType.value }?.value
                ?: ""
//            CollectionType.OTP_DEPOSIT -> "Credit"
            CollectionType.OTP_DEPOSIT -> localAccountList.find { it.name == accountType.value }?.value
                ?: ""
//            CollectionType.OTP_WITHDRAWAL -> "Debit"
            CollectionType.OTP_WITHDRAWAL -> localAccountList.find { it.name == accountType.value }?.value
                ?: ""
        }

    private var _isSubmitButtonEnabled = MediatorLiveData<Boolean>().apply {
        addSource(accountType) {
            value = hasAllRequiredData
        }
        addSource(accountNumber) {
            value = hasAllRequiredData
        }
        addSource(amount) {
            value = hasAllRequiredData
        }
    }

    val isSubmitButtonEnabled: LiveData<Boolean> = _isSubmitButtonEnabled

    private val hasAllRequiredData: Boolean
        get() = if (_collectionType == CollectionType.OTP_WITHDRAWAL)
            !accountNumber.value.isNullOrEmpty() && !amount.value.isNullOrEmpty()
        else !accountType.value.isNullOrEmpty() && !accountNumber.value.isNullOrEmpty()
                && !amount.value.isNullOrEmpty()

    fun setCollectionType(type: CollectionType) {
        _collectionType = type
        if (_collectionType == CollectionType.OTP_WITHDRAWAL) {
            viewModelScope.launch {
                settingsRepository.getAccountsOtp()?.let {
                    localAccountList.addAll(it)
                    _accountList.value = localAccountList.map { it.name }
                }
            }
        }
        isAccountTypeRequired.value = _collectionType == CollectionType.OTP_WITHDRAWAL
    }

    val selectAccountClickEvent = SingleLiveEvent<Void>()
    val fetchAccountInfoFailureEvent = SingleLiveEvent<String>()

    fun onSelectAccountClicked() {
        selectAccountClickEvent.call()
    }

    fun setAccountSearchResult(data: SearchAccountData?) {
        if (data != null) {
            viewModelScope.launch {
                _isAccountDetailsLoading.value = true
                when (val result =
                    accountRepository.getAccountInfo(selectedAccountCode, data.slug)) {
                    is Result.Success -> result.data.also {
                        memberName.value = it.memberName
                        memberNumber.value = it.memberNo
                        memberScheme.value = it.scheme
                        accountNumber.value = it.acNo
                        openDate.value = it.openDate
                        amountBalance.value = it.acBalance ?: ""
                        amountToPay.value = it.amountToCollect
                        _slug = result.data.acSlug
                        hasAccountDetails.value = true
                    }
                    is Result.Error -> fetchAccountInfoFailureEvent.value = result.error
                }
                _isAccountDetailsLoading.value = false
            }
        }
    }

    fun onSubmitButtonClicked() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
//            loading(true)
            val result = accountRepository.submitCollectionOtp(
                selectedAccountCode,
                _slug,
                amount.value ?: "0",
                tType = "debit"
            )
            _otpRequestResultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    fun resendOtp(otpId: String) {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = accountRepository.resendOtp(otpId)
            _otpRequestResultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    fun verifyOtp(otpId: String, otpCode: String?) {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = accountRepository.verifyOtp(otpId, otpCode)
            _verifyOtpResultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    private fun notifyAccountTypeChanged() {
        memberName.value = ""
        memberNumber.value = ""
        memberScheme.value = ""
        accountNumber.value = ""
        openDate.value = ""
        amountBalance.value = ""
        amountToPay.value = ""
        _slug = ""
        hasAccountDetails.value = false
    }

    override fun onCleared() {
        super.onCleared()
        _isSubmitButtonEnabled.removeSource(accountType)
        _isSubmitButtonEnabled.removeSource(accountNumber)
        _isSubmitButtonEnabled.removeSource(amount)
        _isSubmitButtonEnabled.removeSource(remarks)

    }
}