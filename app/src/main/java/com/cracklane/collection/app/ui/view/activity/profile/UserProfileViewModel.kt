package com.cracklane.collection.app.ui.view.activity.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.domain.repository.IUserRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserProfileViewModel @Inject constructor(private val repository: IUserRepository) :
    BaseViewModel() {

    val userId = MutableLiveData<String?>()
    val agentName = MutableLiveData<String?>()
    val agentImage = MutableLiveData<String?>()
    val agentCode = MutableLiveData<String?>()
    val agentRank = MutableLiveData<String?>()
    val mobileNo = MutableLiveData<String?>()
    val enrollmentDate = MutableLiveData<String?>()
    val dob = MutableLiveData<String?>()
    val fatherName = MutableLiveData<String?>()
    val spouseName = MutableLiveData<String?>()
    val nomineeName = MutableLiveData<String?>()
    val nomineeRelation = MutableLiveData<String?>()
    val aadharNo = MutableLiveData<String?>()
    val panNo = MutableLiveData<String?>()
    val permanentAddress = MutableLiveData<String?>()

    init {
        viewModelScope.launch {
            loading(true)
            // load agent details
            when (val result = repository.getUserInfo()) {
                is Result.Success -> {
                    userId.value = result.data.userId
                    agentName.value = result.data.name
                    agentImage.value = result.data.imageUrl
                    agentCode.value = result.data.agentCode
                    agentRank.value = result.data.agentRank
                    mobileNo.value = result.data.mobileNo
                    enrollmentDate.value = result.data.enrollmentDate
                    dob.value = result.data.dob
                    fatherName.value = result.data.fatherName
                    spouseName.value = result.data.spouseName
                    nomineeName.value = result.data.nomineeName
                    nomineeRelation.value = result.data.nomineeRelation
                    aadharNo.value = result.data.aadharNo
                    panNo.value = result.data.panNo
                    permanentAddress.value = result.data.permanentAddress
                }
                is Result.Error -> {}
            }
            loading(false)
        }
    }
}