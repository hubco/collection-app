package com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.inbuilt

import androidx.lifecycle.MutableLiveData
import com.cracklane.collection.app.common.session.ISession
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class InbuiltViewModel @Inject constructor(
    private val session: ISession
): BaseViewModel() {

    val inbuiltLiveList: MutableLiveData<List<InbuiltData>> = MutableLiveData()
    var inbuiltList : ArrayList<InbuiltData> = ArrayList()

    init {
        inbuiltList = INBUILT_PRINTER_LIST
        updatePreSelectedPrinter()
        inbuiltLiveList.value = inbuiltList
    }

    private fun updatePreSelectedPrinter() {
        for(item in inbuiltList){
            if(item.printerCode == session.printerCode &&  item.printerName == session.printerName){
//                item.isPrinterCheck = true
                break
            }
        }
    }
}