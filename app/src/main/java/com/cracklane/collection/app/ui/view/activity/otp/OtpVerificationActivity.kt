package com.cracklane.collection.app.ui.view.activity.otp

import android.os.Bundle
import androidx.activity.viewModels
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.common.extensions.dialog.errorAlert
import com.cracklane.collection.app.common.extensions.dialog.infoAlert
import com.cracklane.collection.app.databinding.ActivityOtpScreenBinding
import com.cracklane.collection.app.ui.navigation.navigateToLogin
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import com.cracklane.collection.app.ui.view.widget.otptextview.OTPListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OtpVerificationActivity : BaseActivity<ActivityOtpScreenBinding>() {

    override val viewModel: OtpVerificationViewModel by viewModels()
    override val layoutRes = R.layout.activity_otp_screen

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // get otp verification data
        viewModel.otpVerificationData.value =
            intent.getParcelableExtra(Constants.EXTRA_OTP_VERIFICATION_DATA)!!

        // register otp event listener
        binding.otpView.otpListener = object : OTPListener {
            override fun onInteractionListener() {
                viewModel.otpCode.value = ""
            }

            override fun onOTPComplete(otp: String) {
                viewModel.otpCode.value = otp
            }
        }
    }

    override fun registerObserver() {
        super.registerObserver()
        viewModel.otpVerificationSuccessEvent.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                infoAlert(it.message) {
                    navigateToLogin()
                }
            }
        }
        viewModel.otpVerificationFailureEvent.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                // clear otp
                binding.otpView.setOTP("")
                // show error message
                errorAlert(it)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        navigateToLogin()
    }
}