package com.cracklane.collection.app.data.remote.service

import com.cracklane.collection.app.data.remote.response.ForgetPasswordResponse
import com.cracklane.collection.app.data.remote.response.GenericResponse
import com.cracklane.collection.app.data.remote.response.LoginResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthService {

    @POST("/api/agent/sign-in")
    @FormUrlEncoded
    suspend fun login(
        @Field("username") userName: String,
        @Field("password") password: String
    ): Response<LoginResponse>

    @POST("/api/agent/sign-in-biometric")
    suspend fun quickLogin(): Response<LoginResponse>

    @POST("/api/agent/v1/forget-password")
    @FormUrlEncoded
    suspend fun forgetPassword(@Field("username") userName: String): Response<ForgetPasswordResponse>

    @POST("/api/agent/v1/change-password")
    @FormUrlEncoded
    suspend fun changePassword(
        @Field("password") password: String,
        @Field("new_password") newPassword: String
    ): Response<GenericResponse>

    @POST("/api/agent/v1/forget-password-verify-otp")
    @FormUrlEncoded
    suspend fun verifyOtp(
        @Field("username") userName: String,
        @Field("otp_id") otpId: String,
        @Field("otp") otpCode: String
    ): Response<GenericResponse>
}