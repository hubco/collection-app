package com.cracklane.collection.app.ui.view.activity.collection.list

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.data.remote.response.CollectionData
import com.cracklane.collection.app.databinding.ActivityCollectionListBinding
import com.cracklane.collection.app.ui.fragment.base.dialog.FILTER_DIALOG
import com.cracklane.collection.app.ui.fragment.base.dialog.FilterDialogFragment
import com.cracklane.collection.app.ui.navigation.Routes
import com.cracklane.collection.app.ui.navigation.navigateTo
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CollectionListActivity : BaseActivity<ActivityCollectionListBinding>() {

    override val viewModel: CollectionListViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_collection_list

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    private val filterDialog by lazy {
        FilterDialogFragment(false) { accountType, _, _, _ ->
            viewModel.getCollectionsForAccount(accountType)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getCollectionsForAccount()

        binding.callback = object : CollectionItemCallback {
            override fun onItemClicked(data: CollectionData) {
                navigateTo(Routes.COLLECTION_SCREEN,
                    mutableMapOf<String, Any>().apply {
                        put(Constants.EXTRA_COLLECTION_DATA, data)
                    })
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_calendar, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_calendar) {
            // show filter
            filterDialog.show(supportFragmentManager, FILTER_DIALOG)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun registerObserver() {}
}

@BindingAdapter("collectionList", "callback", requireAll = false)
fun RecyclerView.setCollections(
    result: List<CollectionData>?,
    callback: CollectionItemCallback?
) {
    result?.let {
        addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayoutManager.VERTICAL
            )
        )
        layoutManager = LinearLayoutManager(context)
        adapter = CollectionListAdapter(it) { data ->
            callback?.onItemClicked(data)
        }
    }
}

interface CollectionItemCallback {
    fun onItemClicked(data: CollectionData)
}