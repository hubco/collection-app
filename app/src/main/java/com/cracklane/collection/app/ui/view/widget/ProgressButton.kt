package com.cracklane.collection.app.ui.view.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.use
import androidx.core.view.isVisible
import com.cracklane.collection.app.R
import com.cracklane.collection.app.databinding.ViewProgresssButtonBinding

class ProgressButton constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet? = null) : this(context, attrs, 0)

    private lateinit var btnText: CharSequence
    private val binding: ViewProgresssButtonBinding by lazy {
        ViewProgresssButtonBinding.inflate(LayoutInflater.from(context), this)
    }

    init {
        context.obtainStyledAttributes(attrs, R.styleable.ProgressButton, 0, 0).use {
            btnText = it.getText(R.styleable.ProgressButton_android_text)
            binding.btnAction.apply {
                isEnabled = it.getBoolean(R.styleable.ProgressButton_android_enabled, isEnabled)
                text = it.getText(R.styleable.ProgressButton_android_text)
            }
        }

        background = ContextCompat.getDrawable(context, R.drawable.selector_progress_button)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return binding.btnAction.dispatchTouchEvent(ev)
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        binding.btnAction.isClickable = enabled
        binding.btnAction.isSelected = enabled
    }

    fun setLoadingVisibility(loading: Boolean) {
        isClickable = !loading
        binding.progressBar.isVisible = loading
        binding.btnAction.isVisible = !loading
    }
}