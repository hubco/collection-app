package com.cracklane.collection.app.data.repository

import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.ForgetPasswordResponse
import com.cracklane.collection.app.data.remote.response.GenericResponse
import com.cracklane.collection.app.data.remote.response.LoginResponse
import com.cracklane.collection.app.data.remote.service.AuthService
import com.cracklane.collection.app.domain.repository.IAuthRepository

class AuthRepository(private val authService: AuthService) : BaseRepository(), IAuthRepository {

    override suspend fun login(userName: String, password: String): Result<LoginResponse> =
        makeNetworkCall { authService.login(userName, password) }

    override suspend fun quickLogin(): Result<LoginResponse> =
        makeNetworkCall { authService.quickLogin() }

    override suspend fun forgetPassword(userName: String): Result<ForgetPasswordResponse> =
        makeNetworkCall { authService.forgetPassword(userName) }

    override suspend fun changePassword(oldPassword: String, newPassword: String) =
        makeNetworkCall { authService.changePassword(oldPassword, newPassword) }

    override suspend fun verifyOtp(
        userName: String,
        otpId: String,
        otpCode: String
    ): Result<GenericResponse> =
        makeNetworkCall { authService.verifyOtp(userName, otpId, otpCode) }

    override suspend fun logout() {
        TODO("Not yet implemented")
    }
}