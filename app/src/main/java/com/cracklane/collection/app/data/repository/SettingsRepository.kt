package com.cracklane.collection.app.data.repository

import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.local.db.dao.AppDao
import com.cracklane.collection.app.data.remote.response.BankAccounts
import com.cracklane.collection.app.data.remote.response.NameValuePair
import com.cracklane.collection.app.data.remote.response.SettingsResponse
import com.cracklane.collection.app.data.remote.service.SettingsService
import com.cracklane.collection.app.domain.repository.ISettingsRepository

class SettingsRepository(
    private val service: SettingsService,
    private val appDao: AppDao
) :
    BaseRepository(), ISettingsRepository {

    override suspend fun loadSettings(): Result<SettingsResponse> =
        makeNetworkCall { service.loadSettings() }.also { result ->
            if (result is Result.Success) {
                if(result.data.bankList.isNullOrEmpty()){
                    result.data.bankList = ArrayList()
                }
                appDao.saveSettings(result.data)
            }
        }

    override suspend fun getAccounts(): List<NameValuePair>? =
        appDao.getSettings()?.accounts

    override suspend fun getAccountsOtp(): List<NameValuePair>? =
        appDao.getSettings()?.accountsOtp

    override suspend fun gettTypes(): List<NameValuePair>? =
        appDao.getSettings()?.tType

    override suspend fun getCompanyName(): String? =
        appDao.getSettings()?.companyName

    override suspend fun getCinNo(): String? = appDao.getSettings()?.cinNo

    override suspend fun getPaymentModes(): List<String>? = appDao.getSettings()?.paymentModes

    override suspend fun getBankAccounts(): List<BankAccounts>? = appDao.getSettings()?.bankAccounts

    override suspend fun getBankList(): List<String>? = appDao.getSettings()?.bankList

    override suspend fun getTransferModes(): List<NameValuePair>? =  appDao.getSettings()?.transferModeList

}