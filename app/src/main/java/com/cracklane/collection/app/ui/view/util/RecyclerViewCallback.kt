package com.cracklane.collection.app.ui.view.util

interface RecyclerViewCallback<T> {
    fun onItemClicked(data: T)
}