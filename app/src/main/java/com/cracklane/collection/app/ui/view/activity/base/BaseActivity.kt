package com.cracklane.collection.app.ui.view.activity.base

import android.annotation.SuppressLint
import android.app.Dialog
import android.location.Location
import android.os.Bundle
import android.util.Log
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.cracklane.collection.app.BR
import com.cracklane.collection.app.CollectionApp
import com.cracklane.collection.app.common.hideKeyboard
import com.cracklane.collection.app.ui.view.widget.ProgressDialog
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

abstract class BaseActivity<VDB : ViewDataBinding> : PermissionActivity() {

    abstract val viewModel: BaseViewModel

    @get:LayoutRes
    abstract val layoutRes: Int

    protected lateinit var binding: VDB
    protected val bindingVariable = BR.viewModel

    private var dialog: Dialog? = null

    /**
     * override this variable and set true to show
     * and handle action bar back button implementation
     */
    @get:LayoutRes
    protected open val toolBarId: Int? = null
    protected open val hasUpAction: Boolean = false

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    // register required observers
    protected open fun registerObserver() {
        viewModel.isLoading.observe(this) {
            if (it) hideKeyboard()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<VDB>(this, layoutRes).apply {
            lifecycleOwner = this@BaseActivity
            setVariable(bindingVariable, viewModel)
            executePendingBindings()
        }

        // setup toolbar
        toolBarId?.let {
            setupToolBar(findViewById(it), hasUpAction)
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        // request location permission
        checkLocationPermission { granted ->
            val app = (application as CollectionApp)
            if (granted && !app.hasLocationData()) {
                showProgressDialog()
                // get last known location
                fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
                    hideProgressDialog()
                    (application as CollectionApp).location = location
                }
            }
        }

        // register observer
        registerObserver()
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()

    }

    protected fun showProgressDialog() {
        hideProgressDialog()
        dialog = ProgressDialog.show(this, false)
    }

    protected fun hideProgressDialog() {
        try {
            dialog?.dismiss()
        } catch (ex: Exception) {
            Log.e("HideProgressDialog", ex.localizedMessage ?: "")
        }
    }
}