package com.cracklane.collection.app.common

import com.cracklane.collection.app.common.session.ISession
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(private val session: ISession) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
        if (session.userId.isNotEmpty() && session.userToken.isNotEmpty()) {
            request.addHeader("User-Id", session.userId)
                .addHeader("Authorization", "Bearer ${session.userToken}")
                .build()
        }
        return chain.proceed(request.build())
    }
}