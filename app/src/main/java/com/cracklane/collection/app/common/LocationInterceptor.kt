package com.cracklane.collection.app.common

import com.cracklane.collection.app.CollectionApp
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class LocationInterceptor @Inject constructor(private val application: CollectionApp) :
    Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val url = request.url.newBuilder()
            .addQueryParameter("lat", "${application.location?.latitude}")
            .addQueryParameter("long", "${application.location?.longitude}")
            .build()

        val requestBuilder = request.newBuilder().url(url)
        return chain.proceed(requestBuilder.build())
//        if (application.hasLocationData()) {
//            request.addHeader("lat", "${application.location?.latitude}")
//                .addHeader("long", "${application.location?.longitude}")
//                .build()
//
//
//        }
//        return chain.proceed(request.build())
    }
}