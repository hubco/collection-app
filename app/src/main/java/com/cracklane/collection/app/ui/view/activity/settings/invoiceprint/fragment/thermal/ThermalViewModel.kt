package com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.thermal

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cracklane.collection.app.data.dataclass.Event
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import com.cracklane.collection.app.ui.view.activity.print.BluetoothHelper
import com.cracklane.collection.app.ui.view.activity.print.BluetoothStatus
import com.dantsu.escposprinter.connection.bluetooth.BluetoothConnections
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ThermalViewModel @Inject constructor(
    val btHelper: BluetoothHelper
) : BaseViewModel() {

    private val _printerList = MutableLiveData<Event<List<String>>>()
    val printerList: LiveData<Event<List<String>>> = _printerList

    init {
        val bluetoothDevicesList = BluetoothConnections().list
        val bluetoothDeviceName = bluetoothDevicesList?.map { it.device.name } ?: emptyList()
        _printerList.value = Event(bluetoothDeviceName)
    }

    fun scanBluetoothDevice() {
        btHelper.scanDevices {
            loading(it == BluetoothStatus.Scanning)
            if (it is BluetoothStatus.Finished) {
                val bluetoothDevicesList = BluetoothConnections().list
                val bluetoothDeviceName =
                    bluetoothDevicesList?.map { it.device.name } ?: emptyList()
                _printerList.value = Event(bluetoothDeviceName)
            }
        }
    }
}