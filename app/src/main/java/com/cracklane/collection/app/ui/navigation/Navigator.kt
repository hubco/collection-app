package com.cracklane.collection.app.ui.navigation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.FragmentActivity
import com.afollestad.inlineactivityresult.startActivityForResult
import java.io.Serializable

const val EXTRA_ACTIVITY_RESULT = "extra_activity_result"

private lateinit var navigationMap: Map<String, Class<*>>

private fun initializeNavigationMap() {
    if (!::navigationMap.isInitialized) {
        navigationMap = Routes.navigationMap
    }
}

fun Context.navigateToLogin() {
    navigateUpTo(Routes.LOGIN_SCREEN, true)
}

fun Activity.navigateToDashboard() {
    navigateUpTo(Routes.DASHBOARD_SCREEN, true)
}

fun Activity.navigateTo(target: String) {
    navigateTo(target, null)
}

private fun Context.navigateUpTo(target: String, newTask: Boolean = false) {
    initializeNavigationMap()
    val clazz = navigationMap[target]
        ?: throw NullPointerException("$target is not registered in navigation map!")

    val intent = Intent(this, clazz).apply {
        if (newTask) {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        } else {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }
    }
    startActivity(intent)
}

fun Activity.navigateTo(target: String, data: Map<String, Any?>? = null) {
    initializeNavigationMap()
    val clazz = navigationMap[target]
        ?: throw NullPointerException("$target is not registered in navigation map!")
    this.startActivity(Intent(this, clazz).apply {
        fillIntentExtra(this, data)
    })
}

private fun fillIntentExtra(intent: Intent, data: Map<String, Any?>? = null) {
    data?.let {
        it.forEach { map ->
            when (map.value) {
                is Int? -> intent.putExtra(map.key, map.value as? Int)
                is Byte? -> intent.putExtra(map.key, map.value as? Byte)
                is Char? -> intent.putExtra(map.key, map.value as? Char)
                is Long? -> intent.putExtra(map.key, map.value as? Long)
                is Float? -> intent.putExtra(map.key, map.value as? Float)
                is Short? -> intent.putExtra(map.key, map.value as? Short)
                is Double? -> intent.putExtra(map.key, map.value as? Double)
                is Boolean? -> intent.putExtra(map.key, map.value as? Boolean)
                is Bundle? -> intent.putExtra(map.key, map.value as? Bundle)
                is String? -> intent.putExtra(map.key, map.value as? String)
                is IntArray? -> intent.putExtra(map.key, map.value as? IntArray)
                is ByteArray? -> intent.putExtra(map.key, map.value as? ByteArray)
                is CharArray? -> intent.putExtra(map.key, map.value as? CharArray)
                is LongArray? -> intent.putExtra(map.key, map.value as? LongArray)
                is FloatArray? -> intent.putExtra(map.key, map.value as? FloatArray)
                is ShortArray? -> intent.putExtra(map.key, map.value as? ShortArray)
                is DoubleArray? -> intent.putExtra(map.key, map.value as? DoubleArray)
                is BooleanArray? -> intent.putExtra(map.key, map.value as? BooleanArray)
                is CharSequence? -> intent.putExtra(map.key, map.value as? CharSequence)
                is Parcelable? -> intent.putExtra(map.key, map.value as? Parcelable)
                is Serializable? -> intent.putExtra(map.key, map.value as? Serializable)
                is Array<*>? -> intent.putExtra(map.key, map.value as? Array<*>)
                else -> throw UnsupportedOperationException("${map.value} is not supported for intent")
            }
        }
    }
}

fun Activity.navigateToAndFinish(target: String) {
    navigateToAndFinish(target, null)
}

fun Activity.navigateToAndFinish(target: String, data: Map<String, Any?>? = null) {
    navigateTo(target, data)
    finish()
}

inline fun <reified T : Activity, D : Parcelable> FragmentActivity.navigateForResult(
    bundle: Bundle,
    crossinline callback: (success: Boolean, data: D?) -> Unit
) {
    startActivityForResult<T>(bundle) { success, data ->
        callback.invoke(success, data.getParcelableExtra<D>(EXTRA_ACTIVITY_RESULT))
    }
}

inline fun <reified D : Parcelable> Activity.setResult(data: D?) {
    setResult(Activity.RESULT_OK, Intent().apply {
        putExtra(EXTRA_ACTIVITY_RESULT, data)
    })
    finish()
}