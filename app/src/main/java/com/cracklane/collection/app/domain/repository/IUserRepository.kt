package com.cracklane.collection.app.domain.repository

import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.AgentInfoResponse

interface IUserRepository {

    suspend fun getUserInfo() : Result<AgentInfoResponse>
}