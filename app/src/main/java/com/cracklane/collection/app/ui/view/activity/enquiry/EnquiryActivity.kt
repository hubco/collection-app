package com.cracklane.collection.app.ui.view.activity.enquiry

import android.app.AlertDialog
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.content.FileProvider
import androidx.lifecycle.lifecycleScope
import com.cracklane.collection.app.BuildConfig
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.extensions.dialog.datePickerDialog
import com.cracklane.collection.app.common.extensions.dialog.errorAlert
import com.cracklane.collection.app.common.extensions.dialog.infoAlert
import com.cracklane.collection.app.common.extensions.dialog.otpAlert
import com.cracklane.collection.app.common.toBase64
import com.cracklane.collection.app.databinding.ActivityEnquiryBinding
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File


@AndroidEntryPoint
class EnquiryActivity : BaseActivity<ActivityEnquiryBinding>() {

    override val viewModel: EnquiryViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_enquiry

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true
    private var otpAlertDialog: AlertDialog? = null

    private val tempImageUri by lazy {
        val tmpFile = File.createTempFile("tmp_image_file", ".jpg", cacheDir).apply {
            createNewFile()
            deleteOnExit()
        }
        FileProvider.getUriForFile(
            applicationContext,
            "${BuildConfig.APPLICATION_ID}.provider",
            tmpFile
        )
    }

    private val cameraLauncher = registerForActivityResult(
        ActivityResultContracts.TakePicture()
    ) { captured ->
        if (captured) {
            showProgressDialog()
            lifecycleScope.launch(Dispatchers.IO) {
                val bitmap =
                    BitmapFactory.decodeStream(contentResolver.openInputStream(tempImageUri))
                viewModel.photo = bitmap.toBase64()
                withContext(Dispatchers.Main) {
                    hideProgressDialog()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.tvAccountType.setOnClickListener {
            binding.spnAccountType.performClick()
        }

        binding.tvDob.setOnClickListener {
            datePickerDialog {
                viewModel.dob.value = it
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_camera, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_camera) {
            cameraLauncher.launch(tempImageUri)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun registerObserver() {
        super.registerObserver()

        viewModel.otpGeneratedEvent.observe(this) {
            otpAlertDialog =
                otpAlert(it.message, "") { isResendOtp, otpCode ->
                    when (isResendOtp) {
                        true -> {
                            otpAlertDialog?.dismiss()
                            showProgressDialog()
                            viewModel.regenerateOtp()
                        }
                        false -> {
                            showProgressDialog()
                            viewModel.verifyOtp(it.otpId, otpCode!!)
                        }
                    }
                }
        }
        viewModel.otpGenerationFailureEvent.observe(this) {
            errorAlert(it)
        }
        viewModel.otpVerificationInProgress.observe(this) {
            if (it) showProgressDialog()
            else hideProgressDialog()
        }
        viewModel.otpVerificationSuccessEvent.observe(this) {
            otpAlertDialog?.dismiss()
            infoAlert(it) { finish() }
        }
        viewModel.otpVerificationFailureEvent.observe(this) {
            errorAlert(it)
        }
    }
}