package com.cracklane.collection.app.ui.view.activity.otp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.data.dataclass.Event
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.request.OtpVerificationRequest
import com.cracklane.collection.app.data.remote.response.GenericResponse
import com.cracklane.collection.app.domain.repository.IAuthRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OtpVerificationViewModel @Inject constructor(private val repository: IAuthRepository) :
    BaseViewModel() {

    val otpVerificationData = MutableLiveData<OtpVerificationRequest>()
    val otpCode = MutableLiveData<String>()

    val submitButtonEnabled = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(otpCode) {
            value = !otpCode.value.isNullOrEmpty()
        }
    }

    private val _otpVerificationSuccessEvent = MutableLiveData<Event<GenericResponse>>()
    val otpVerificationSuccessEvent: LiveData<Event<GenericResponse>> = _otpVerificationSuccessEvent

    private val _otpVerificationFailureEvent = MutableLiveData<Event<String>>()
    val otpVerificationFailureEvent: LiveData<Event<String>> = _otpVerificationFailureEvent

    fun onSubmitButtonClick() {
        viewModelScope.launch {
            loading(true)
            when (val result = repository.verifyOtp(
                otpVerificationData.value!!.userName,
                otpVerificationData.value!!.otpId,
                otpCode.value!!
            )) {
                is Result.Success -> _otpVerificationSuccessEvent.value = Event(result.data)
                is Result.Error -> _otpVerificationFailureEvent.value = Event(result.error)
            }
            loading(false)
        }
    }

    override fun onCleared() {
        super.onCleared()
        submitButtonEnabled.removeSource(otpCode)
    }
}