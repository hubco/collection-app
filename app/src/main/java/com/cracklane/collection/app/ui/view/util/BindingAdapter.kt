package com.cracklane.collection.app.ui.view.util

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.cracklane.collection.app.R
import com.cracklane.collection.app.ui.view.widget.ProgressButton
import java.util.*

@BindingAdapter("isLoading")
fun setVisibility(view: ProgressButton, isLoading: Boolean?) {
    view.setLoadingVisibility(isLoading ?: false)
}

@BindingAdapter("android:visibility")
fun setVisibility(view: View, visible: Boolean?) {
    view.isVisible = visible ?: false
}

@BindingAdapter("dataList")
fun setSpinnerAdapter(view: Spinner, list: List<String>?) {
    list?.let { accountList ->
        view.adapter = ArrayAdapter(
            view.context,
            R.layout.layout_list_item,
            accountList.map {
                it.replaceFirstChar { char ->
                    if (char.isLowerCase()) char.titlecase(
                        Locale.getDefault()
                    ) else char.toString()
                }
            }).apply {
            setDropDownViewResource(android.R.layout.simple_list_item_1)
        }
    }
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter(value = ["selectedValue", "selectedValueAttrChanged"], requireAll = false)
fun bindSpinnerData(
    spinner: Spinner,
    newSelectedValue: String?,
    newTextAttrChanged: InverseBindingListener
) {
    spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>?,
            view: View,
            position: Int,
            id: Long
        ) {
            newTextAttrChanged.onChange()
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {}
    }

    newSelectedValue?.let { value ->
        (spinner.adapter as? ArrayAdapter<Any>?)?.getPosition(value)?.let {
            spinner.setSelection(it, true)
        }
    }
}

@InverseBindingAdapter(attribute = "selectedValue", event = "selectedValueAttrChanged")
fun captureSelectedSpinnerValue(spinner: Spinner): Any? {
    return spinner.selectedItem
}

@BindingAdapter("formValidator")
fun formValidator(editText: AppCompatEditText, errorText: String?) {
    // ignore infinite loops
    if (TextUtils.isEmpty(errorText)) {
        editText.error = null
        return
    }
    editText.error = errorText
}

@SuppressLint("CheckResult")
@BindingAdapter("imageUrl", "circleCrop", "placeholder", "error", requireAll = false)
fun ImageView.bindSrcUrl(
    url: String?,
    circleCrop: Boolean,
    placeholder: Drawable?,
    errorDrawable: Drawable?
) {
    Glide.with(this).load(url).run {
        if (circleCrop) apply(RequestOptions.circleCropTransform())
        if (placeholder != null) placeholder(placeholder)
        if (errorDrawable != null) error(errorDrawable)
        into(this@bindSrcUrl)
    }
}

@BindingAdapter("paymentModes")
fun RadioGroup.bindPaymentModes(paymentModes: List<String>?) {
    paymentModes?.forEachIndexed { index, value ->
        val radioButton = RadioButton(context).apply {
            id = index
            text = value
            isChecked = index == 0
        }
        addView(radioButton)
    }
}

@BindingAdapter("refreshing", "refreshCallback", requireAll = false)
fun SwipeRefreshLayout.bindRefreshStatus(loading: Boolean, callback: (() -> Unit)?) {
    isRefreshing = loading
    setOnRefreshListener { callback?.invoke() }
}