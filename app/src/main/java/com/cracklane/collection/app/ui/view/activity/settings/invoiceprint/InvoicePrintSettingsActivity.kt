package com.cracklane.collection.app.ui.view.activity.settings.invoiceprint

import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import com.cracklane.collection.app.R
import com.cracklane.collection.app.databinding.ActivityInvoicePrintSettingsBinding
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import com.cracklane.collection.app.ui.view.activity.print.BluetoothUtil_
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_invoice_print_settings.*


@AndroidEntryPoint
class InvoicePrintSettingsActivity : BaseActivity<ActivityInvoicePrintSettingsBinding>() {

    override val viewModel: InvoicePrintSettingsViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_invoice_print_settings
    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    private lateinit var adapter: InvoicePrintPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val fragmentManager: FragmentManager = supportFragmentManager

        adapter = InvoicePrintPagerAdapter(this, fragmentManager)
        viewPager.adapter = adapter

        tabLayout.setupWithViewPager(viewPager)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewPager.currentItem = tab!!.position
                when (tab.position) {
                    1 -> {
                        if (BluetoothUtil_.isBluetoothEnabled(this@InvoicePrintSettingsActivity) == false) {
                            BluetoothUtil_.enableBluetooth(this@InvoicePrintSettingsActivity)
                        }
                    }
                }
            }
        })
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                /* if( viewPager?.adapter != null){
                     viewPager?.adapter?.notifyDataSetChanged()
                 }*/
            }

            override fun onPageScrollStateChanged(state: Int) {
            }

        })
    }

    fun notifyPrinterChanged() {
        val notifyTabIndex = if(viewPager.currentItem == 0) 1 else 0
        adapter.getItem(notifyTabIndex).notifyPrinterChanged()
    }
}