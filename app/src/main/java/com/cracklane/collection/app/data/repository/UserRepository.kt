package com.cracklane.collection.app.data.repository

import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.AgentInfoResponse
import com.cracklane.collection.app.data.remote.service.UserService
import com.cracklane.collection.app.domain.repository.IUserRepository

class UserRepository(
    private val service: UserService
) : BaseRepository(), IUserRepository {

    override suspend fun getUserInfo(): Result<AgentInfoResponse> = makeNetworkCall {
        service.getAccountInfo()
    }
}