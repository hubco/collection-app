package com.cracklane.collection.app.domain.repository

import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.*

interface IAccountRepository {

    suspend fun searchAccount(accountType: String, query: String): Result<SearchAccountResponse>

    suspend fun getAccountInfo(accountType: String, slug: String): Result<AccountInfoResponse>

    suspend fun submitCollection(
        accountType: String,
        slug: String,
        amount: String,
        pMode: String,
        utr_no: String,
        t_date: String,
        bank_account_id: String,
        t_mode: String,
        bank_name: String,
        remarks: String
    ): Result<CollectionSubmitResponse>

    suspend fun submitCollectionOtp(
        accountType: String,
        slug: String,
        amount: String,
        tType: String
    ): Result<CollectionSubmitOtpResponse>

    suspend fun resendOtp(otpId: String): Result<CollectionSubmitOtpResponse>

    suspend fun verifyOtp(otpId: String, otpCode: String?): Result<CollectionSubmitResponse>

    suspend fun homePageLimit(): Result<HomePageLimitResponse>

    suspend fun getTransactionList(
        accountType: String,
        transactionType: String,
        fromDate: String,
        toDate: String
    ): Result<TransactionResponse>

    suspend fun getCollectionList(accountType: String, date: String): Result<CollectionResponse>
}