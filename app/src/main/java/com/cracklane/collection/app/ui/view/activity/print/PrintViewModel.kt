package com.cracklane.collection.app.ui.view.activity.print

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.common.architecture.SingleLiveEvent
import com.cracklane.collection.app.common.session.ISession
import com.cracklane.collection.app.data.remote.response.AccountInfoResponse
import com.cracklane.collection.app.data.remote.response.CollectionSubmitResponse
import com.cracklane.collection.app.data.remote.response.TransactionData
import com.cracklane.collection.app.domain.repository.ISettingsRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class PrintViewModel @Inject constructor(
    private val settingsRepository: ISettingsRepository,
    private val session: ISession
) : BaseViewModel() {

    val printClickEvent = SingleLiveEvent<Void>()

    fun onPrintClicked() {
        printClickEvent.call()
    }

    val memberCompanyName = MutableLiveData<String>()
    val memberCinNo = MutableLiveData<String>()
    val memberRegNo = MutableLiveData<String>()
    val memberName = MutableLiveData<String>()
    val memberAcNo = MutableLiveData<String>()
    val memberPhone = MutableLiveData<String>()
    val date = MutableLiveData<String>()
    val refId = MutableLiveData<String>()
    val amount = MutableLiveData<String>() // amount is deposited amount
    val mode = MutableLiveData<String>()
    val status = MutableLiveData<String>()
    val availBal = MutableLiveData<String>()
    val remarks = MutableLiveData<String?>()
    val printedBy = MutableLiveData<String>()
    val printedOn = MutableLiveData<String>()

    fun setTransactionData(data: TransactionData?) {
        data?.let {
            viewModelScope.launch {
                settingsRepository.getCompanyName()?.let {
                    memberCompanyName.value = it
                }
                settingsRepository.getCinNo()?.let {
                    memberCinNo.value = it
                }

                refId.value = data.tId
                availBal.value = data.acBalance
                mode.value = data.pMode
                date.value = data.tDate
                status.value = data.tStatus
                amount.value = data.amount
                remarks.value = data.remarks

                val dateTime = Calendar.getInstance().time.toString()
                memberRegNo.value = data.memberNo
                memberName.value = data.memberName
                memberAcNo.value = data.acNo
                memberPhone.value = data.mobileNo
                printedBy.value = session.userName
                printedOn.value = dateTime
            }
        }
    }

    fun setAccountResult(data: AccountInfoResponse?) {
        data?.let {
            viewModelScope.launch {
                settingsRepository.getCompanyName()?.let {
                    memberCompanyName.value = it
                }
                settingsRepository.getCinNo()?.let {
                    memberCinNo.value = it
                }
                val dateTime = Calendar.getInstance().time.toString()
                memberRegNo.value = data.memberNo
                memberName.value = data.memberName
                memberAcNo.value = data.acNo
                memberPhone.value = data.mobileNo
                printedBy.value = session.userName
                printedOn.value = dateTime
            }
        }
    }

    fun setCollectionResult(data: CollectionSubmitResponse?) {
        data?.let {
            refId.value = data.transactionId
            availBal.value = data.accountBalance
            mode.value = data.paymentMode
            date.value = data.transactionDate
            status.value = data.paymentStatus
        }
    }
}