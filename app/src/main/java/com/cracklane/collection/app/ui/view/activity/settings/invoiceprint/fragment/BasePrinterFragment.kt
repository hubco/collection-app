package com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.cracklane.collection.app.ui.fragment.base.BaseFragment

abstract class BasePrinterFragment<VM : ViewModel, VDB : ViewDataBinding> : BaseFragment<VM, VDB>() {

    abstract fun notifyPrinterChanged()
}