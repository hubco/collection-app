package com.cracklane.collection.app.ui.view.activity.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.TransactionData
import com.cracklane.collection.app.domain.repository.IAccountRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(private val repository: IAccountRepository) :
    BaseViewModel() {

    private val _transactionList = MutableLiveData<List<TransactionData>>()
    val transactionList: LiveData<List<TransactionData>> = _transactionList

    private val _errorMessage = MutableLiveData<String?>()
    val errorMessage: LiveData<String?> = _errorMessage

    fun getTransactions(
        accountType: String = "",
        transactionType: String = "",
        fromDate: String = "",
        toDate: String = ""
    ) {
        viewModelScope.launch {
            _errorMessage.value = null
            _transactionList.value = emptyList()

            loading(true)
            val calendar = Calendar.getInstance()
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            val month = calendar.get(Calendar.MONTH)
            val year = calendar.get(Calendar.YEAR)

            val fromDateDelegate: String = if (fromDate.isEmpty())
                "$day/$month/$year"
            else fromDate

            val toDateDelegate: String = if (fromDate.isEmpty())
                "$day/$month/$year"
            else toDate

            when (val result =
                repository.getTransactionList(accountType, transactionType, fromDateDelegate, toDateDelegate)) {
                is Result.Success -> {
                    _transactionList.value = result.data.response
                    if (result.data.response.isEmpty()) {
                        _errorMessage.value = "No result found!"
                    }
                }
                is Result.Error -> _errorMessage.value = result.error
            }
            loading(false)
        }
    }
}