package com.cracklane.collection.app.data.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CollectionSubmitOtpResponse(
    val status: String,
    @SerializedName("otp_id")
    val otpId: String,
    val message: String
): Parcelable