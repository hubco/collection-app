package com.cracklane.collection.app.common.architecture

import androidx.lifecycle.MutableLiveData

class NotifyChangeLiveData<T>(private val callback: () -> Unit) : MutableLiveData<T>() {

    private var _oldValue: T? = null

    override fun setValue(value: T?) {
        compareAndNotify(value)
        super.setValue(value)
    }

    override fun postValue(value: T?) {
        compareAndNotify(value)
        super.postValue(value)
    }

    private fun compareAndNotify(value: T?) {
        if (_oldValue != value) {
            _oldValue = value
            callback()
        }
    }
}