package com.cracklane.collection.app.ui.view.activity.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cracklane.collection.app.data.remote.response.SearchAccountData
import com.cracklane.collection.app.databinding.LayoutSearchItemBinding

class SearchListAdapter(private val list: List<SearchAccountData>,
                        private val callback: ((data: SearchAccountData) -> Unit)? = null) :
    RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchListAdapter.ViewHolder {
        val binding = LayoutSearchItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: SearchListAdapter.ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class ViewHolder(val binding: LayoutSearchItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SearchAccountData) {
            binding.text1.text = item.text
            binding.root.setOnClickListener {
                callback?.invoke(item)
            }
        }
    }
}