package com.cracklane.collection.app.ui.fragment.base.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModel
import com.cracklane.collection.app.BR

abstract class BaseDialogFragment<VM : ViewModel, VDB : ViewDataBinding> : DialogFragment() {

    abstract val viewModel: ViewModel

    @get:LayoutRes
    abstract val layoutRes: Int

    protected lateinit var dataBinding: VDB
    protected val bindingVariable = BR.viewModel

    protected open fun registerObserver() {}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate<VDB>(inflater, layoutRes, container, false).apply {
            lifecycleOwner = this@BaseDialogFragment
            setVariable(bindingVariable, viewModel)
            executePendingBindings()
        }
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerObserver()
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }
}