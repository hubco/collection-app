package com.cracklane.collection.app.domain.repository

import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.ForgetPasswordResponse
import com.cracklane.collection.app.data.remote.response.GenericResponse
import com.cracklane.collection.app.data.remote.response.LoginResponse

interface IAuthRepository {

    suspend fun login(userName: String, password: String) : Result<LoginResponse>

    suspend fun quickLogin(): Result<LoginResponse>

    suspend fun forgetPassword(userName: String) : Result<ForgetPasswordResponse>

    suspend fun changePassword(oldPassword: String, newPassword: String) : Result<GenericResponse>

    suspend fun verifyOtp(userName: String, otpId: String, otpCode: String) : Result<GenericResponse>

    suspend fun logout()
}