package com.cracklane.collection.app.common

import android.util.Log
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.cracklane.collection.app.CollectionApp
import com.cracklane.collection.app.R

private const val TAG = "Biometric"

object Biometric {

    val isAvailable: Boolean
        get() {
            val biometricManager = BiometricManager.from(CollectionApp.appContext)
            return when (biometricManager.canAuthenticate(BIOMETRIC_STRONG)) {
                BiometricManager.BIOMETRIC_SUCCESS,
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> true
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE,
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> false
                else -> false
            }
        }

    fun authenticate(
        activity: FragmentActivity,
        callback: (success: Boolean, error: CharSequence?) -> Unit
    ) {
        BiometricPrompt(activity, ContextCompat.getMainExecutor(activity),
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    Log.d(TAG, "Authentication error: $errString")
                    callback.invoke(false, errString)
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
                    callback.invoke(true, null)
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    callback.invoke(false, null)
                }
            }).authenticate(
            BiometricPrompt.PromptInfo.Builder()
                .setTitle(activity.getString(R.string.label_quick_login))
                .setSubtitle(activity.getString(R.string.label_quick_login_info))
                .setNegativeButtonText(activity.getString(R.string.label_cancel))
                .setAllowedAuthenticators(BIOMETRIC_STRONG)
                .build()
        )
    }
}