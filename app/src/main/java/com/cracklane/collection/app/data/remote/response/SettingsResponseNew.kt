package com.cracklane.collection.app.data.remote.response

data class SettingsResponseNew(
    val account_types: List<List<String>>,
    val account_types_otp: List<List<String>>,
    val printer_types: List<String>,
    val status: String,
    val t_types: List<List<String>>
)