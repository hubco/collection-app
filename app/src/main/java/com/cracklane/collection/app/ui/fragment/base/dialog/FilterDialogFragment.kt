package com.cracklane.collection.app.ui.fragment.base.dialog

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.cracklane.collection.app.R
import com.cracklane.collection.app.databinding.DialogFilterBinding
import dagger.hilt.android.AndroidEntryPoint

const val FILTER_DIALOG = "FilterDialogFragment"

@AndroidEntryPoint
class FilterDialogFragment(
    private val showAllFilters: Boolean = true,
    private val callback: (accountType: String, transactionType: String, startDate: String, endDate: String) -> Unit
) :
    BaseDialogFragment<FilterViewModel, DialogFilterBinding>() {

    override val viewModel: FilterViewModel by viewModels()
    override val layoutRes: Int = R.layout.dialog_filter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.isTransactionListFilter.value = showAllFilters

        dataBinding.btnClose.setOnClickListener {
            dismiss()
        }

        dataBinding.radioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rbAll -> viewModel.type = ""
                R.id.rbCredit -> viewModel.type = "credit"
                R.id.rbDebit -> viewModel.type = "debit"
            }
        }

        dataBinding.btnApply.setOnClickListener {
            callback(
                viewModel.accountCode,
                viewModel.type,
                viewModel.fromDate.value!!,
                viewModel.toDate.value!!
            )
            dismiss()
        }

        dataBinding.btnReset.setOnClickListener {
            viewModel.setupTodayDate()
            callback("", viewModel.type, viewModel.fromDate.value!!, viewModel.toDate.value!!)
            dismiss()
        }

        dataBinding.tvFromDate.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                { _, year, monthOfYear, dayOfMonth ->
                    viewModel.fromDay = dayOfMonth
                    viewModel.fromMonth = monthOfYear + 1
                    viewModel.fromYear = year
                    viewModel.isFromDateChanged = true
                    viewModel.setToDateAsFromDate()
                },
                viewModel.fromYear,
                if (viewModel.isFromDateChanged) viewModel.fromMonth - 1 else viewModel.fromMonth,
                viewModel.fromDay
            ).show()
        }

        dataBinding.tvToDate.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                { _, year, monthOfYear, dayOfMonth ->
                    viewModel.toDay = dayOfMonth
                    viewModel.toMonth = monthOfYear + 1
                    viewModel.toYear = year
                    viewModel.isToDateChanged = true
                    viewModel.updateDates()
                },
                viewModel.toYear,
                if (viewModel.isToDateChanged) viewModel.toMonth - 1 else viewModel.toMonth,
                viewModel.toDay
            ).apply {
                datePicker.minDate = viewModel.getToMinDate()
            }.show()
        }
    }
}