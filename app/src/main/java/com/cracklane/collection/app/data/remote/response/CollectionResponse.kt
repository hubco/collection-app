package com.cracklane.collection.app.data.remote.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class CollectionResponse(
    val response: List<CollectionData>,
    val status: String
)

@Parcelize
data class CollectionData(
    val acBalance: String?,
    val acNo: String,
    val acSlug: String,
    val acType: String,
    val amountToCollect: Int,
    val dueDate: String,
    val installmentDue: Int,
    val memberName: String,
    val memberNo: String,
    val mobileNo: String,
    val openDate: String,
    val scheme: String
) : Parcelable

fun CollectionData.toAccountInfo() : AccountInfoResponse = AccountInfoResponse(
    acBalance = "$acBalance",
    acNo = acNo,
    acSlug = acSlug,
    amountToCollect = "$amountToCollect",
    memberName = memberName,
    memberNo = memberNo,
    mobileNo = mobileNo,
    openDate = openDate,
    scheme = scheme
)