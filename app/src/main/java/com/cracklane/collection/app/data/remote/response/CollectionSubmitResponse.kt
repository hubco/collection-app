package com.cracklane.collection.app.data.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CollectionSubmitResponse(
    val status: String,
    @SerializedName("t_id")
    val transactionId: String,
    @SerializedName("t_date")
    val transactionDate: String,
    @SerializedName("p_mode")
    val paymentMode: String,
    @SerializedName("t_status")
    val paymentStatus: String,
    @SerializedName("ac_balance")
    val accountBalance: String
) : Parcelable