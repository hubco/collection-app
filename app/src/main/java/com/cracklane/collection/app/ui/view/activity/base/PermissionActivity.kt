package com.cracklane.collection.app.ui.view.activity.base

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import com.cracklane.collection.app.BuildConfig
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.extensions.dialog.confirmAlert
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener
import com.karumi.dexter.listener.single.PermissionListener

abstract class PermissionActivity : CoreActivity() {

    private val genericPermissionListener by lazy {
        DialogOnDeniedPermissionListener.Builder
            .withContext(this)
            .withTitle(getString(R.string.permission_rational_title))
            .withMessage(getString(R.string.permission_rational_message))
            .withButtonText(android.R.string.ok) {
                openAppSettings()
                finishAffinity()
            }
            .withIcon(R.drawable.ic_alert)
            .build()
    }

    protected fun openAppSettings() {
        startActivity(
            Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + BuildConfig.APPLICATION_ID)
            )
        )
    }

    protected fun checkLocationPermission(callback: (granted: Boolean) -> Unit) {
        Dexter.withContext(this)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    callback.invoke(true)
                    genericPermissionListener.onPermissionGranted(response)
                }

                override fun onPermissionRationaleShouldBeShown(
                    request: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    genericPermissionListener.onPermissionRationaleShouldBeShown(request, token)
//                    showPermissionDeniedDialog()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    callback.invoke(false)
                    genericPermissionListener.onPermissionDenied(response)
//                    showPermissionDeniedDialog()
                }
            }).check()
    }

    private fun showPermissionDeniedDialog() {
        confirmAlert(getString(R.string.permission_rational_message)) { confirmed ->
            if (confirmed) {
                startActivity(
                    Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                    )
                )
            } else {
                finishAndRemoveTask()
            }
        }
    }
}