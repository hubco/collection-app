package com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.thermal

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cracklane.collection.app.common.session.ISession
import com.cracklane.collection.app.databinding.PrinterCardViewBinding
import javax.inject.Inject

class ThermalPrinterAdapter @Inject constructor(
    val session: ISession,
    val callback: ()->Unit
) : RecyclerView.Adapter<ThermalPrinterAdapter.PrinterItemViewHolder>() {

    private var printerList = listOf<String?>()

    private var lastSelectedPosition = -1

    fun setPrinterList(list: List<String?>) {
        this.printerList = list
        notifyDataSetChanged()
    }

    fun refreshSelectedPrinter() {
        lastSelectedPosition = -1
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PrinterItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = PrinterCardViewBinding.inflate(layoutInflater, parent, false)
        return PrinterItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PrinterItemViewHolder, position: Int) {
        val printer = printerList[position]
        holder.binding.printerName = printer
        holder.binding.executePendingBindings()

        if (lastSelectedPosition == -1) {
            holder.binding.switchView.isChecked = session.printerName == printer
        } else {
            holder.binding.switchView.isChecked = (lastSelectedPosition == position)
        }

        holder.binding.root.setOnClickListener {
            refreshData(holder, printer ?: "")
        }
        holder.binding.switchView.setOnClickListener {
            refreshData(holder, printer ?: "")
        }
    }

    private fun refreshData(
        holder: PrinterItemViewHolder,
        printer: String
    ) {
        lastSelectedPosition = holder.adapterPosition
        notifyDataSetChanged()
        session.printerName = printer
        session.printerCode = ""
        session.isInbuiltPriterSelected = false
        callback()
    }

    override fun getItemCount(): Int = printerList.size

    inner class PrinterItemViewHolder(
        val binding: PrinterCardViewBinding,
    ) : RecyclerView.ViewHolder(binding.root)

}