package com.cracklane.collection.app.data.remote.service

import com.cracklane.collection.app.data.remote.response.SettingsResponse
import retrofit2.Response
import retrofit2.http.GET

interface SettingsService {

    @GET("/api/agent/v1/app-settings")
    suspend fun loadSettings(): Response<SettingsResponse>
}