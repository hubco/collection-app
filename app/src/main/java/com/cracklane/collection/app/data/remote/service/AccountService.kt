package com.cracklane.collection.app.data.remote.service

import com.cracklane.collection.app.data.remote.response.*
import retrofit2.Response
import retrofit2.http.*

interface AccountService {

    @GET("/api/agent/v1/account-search")
    suspend fun searchAccount(
        @Query("ac_type") accountType: String,
        @Query("ac_no") query: String
    ): Response<SearchAccountResponse>

    @GET("/api/agent/v1/account-info")
    suspend fun getAccountInfo(
        @Query("ac_type") accountType: String,
        @Query("ac_slug") slug: String
    ): Response<AccountInfoResponse>

    @POST("/api/agent/v1/post-entry")
    @FormUrlEncoded
    suspend fun submitCollection(
        @Field("ac_type") accountType: String,
        @Field("ac_slug") slug: String,
        @Field("amount") amount: String,
        @Field("p_mode") mode: String,
        @Field("utr_no") utrNumber: String,
        @Field("t_date") transactionDate: String,
        @Field("bank_id") bankAccountId: String,
        @Field("t_mode") transactionMode: String,
        @Field("bank_name") bankName: String,
        @Field("message") remarks: String,
    ): Response<CollectionSubmitResponse>

    @POST("/api/agent/v1/post-entry/initiate-otp")
    @FormUrlEncoded
    suspend fun submitCollectionOtp(
        @Field("ac_type") accountType: String,
        @Field("ac_slug") slug: String,
        @Field("amount") amount: String,
        @Field("t_type") type: String
    ): Response<CollectionSubmitOtpResponse>

    @POST("/api/agent/v1/post-entry/resent-otp")
    @FormUrlEncoded
    suspend fun resendOtp(
        @Field("otp_id") otpId: String
    ): Response<CollectionSubmitOtpResponse>

    @POST("/api/agent/v1/post-entry/verify-otp")
    @FormUrlEncoded
    suspend fun verifyOtp(
        @Field("otp_id") otpId: String,
        @Field("otp") otpCode: String?
    ): Response<CollectionSubmitResponse>

    @GET("/api/agent/v1/agent-limits")
    suspend fun getHomePageLimits(): Response<HomePageLimitResponse>

    @GET("/api/agent/v1/transaction-list")
    suspend fun getTransactionList(
        @Query("ac_type") accountType: String,
        @Query("t_type") transactionType: String,
        @Query("s_date") fromDate: String,
        @Query("e_date") toDate: String
    ): Response<TransactionResponse>

    @GET("/api/agent/v1/collection-list")
    suspend fun getCollectionList(@Query("ac_type") accountType: String, @Query("date") date: String): Response<CollectionResponse>
}