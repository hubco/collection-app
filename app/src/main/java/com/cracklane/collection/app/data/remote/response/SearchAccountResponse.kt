package com.cracklane.collection.app.data.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class SearchAccountResponse(
    val results: List<SearchAccountData>,
    val pagination: Boolean
)

@Parcelize
data class SearchAccountData(
    val id: String,
    val text: String,
    @SerializedName("data-slug")
    val slug: String
) : Parcelable
