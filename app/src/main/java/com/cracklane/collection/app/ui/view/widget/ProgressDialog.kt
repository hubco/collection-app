package com.cracklane.collection.app.ui.view.widget

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import com.cracklane.collection.app.R
import com.cracklane.collection.app.databinding.LayoutProgressBarBinding

object ProgressDialog {

    fun show(
        context: Context, cancelable: Boolean = true
    ): Dialog {
        val binding = LayoutProgressBarBinding.inflate(LayoutInflater.from(context))
        return Dialog(context, R.style.FullScreenProgressDialog).apply {
            setContentView(binding.root)
            setCancelable(cancelable)
            show()
        }
    }

    fun create(
        context: Context, cancelable: Boolean = true
    ): Dialog {
        val binding = LayoutProgressBarBinding.inflate(LayoutInflater.from(context))
        return Dialog(context, R.style.FullScreenProgressDialog).apply {
            setContentView(binding.root)
            setCancelable(cancelable)
        }
    }
}