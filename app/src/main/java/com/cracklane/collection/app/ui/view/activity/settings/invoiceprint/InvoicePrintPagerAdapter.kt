package com.cracklane.collection.app.ui.view.activity.settings.invoiceprint

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.cracklane.collection.app.R
import com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.BasePrinterFragment
import com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.inbuilt.InbuiltFragment
import com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.thermal.ThermalFragment

class InvoicePrintPagerAdapter(
    context: Context,
    fragmentManager: FragmentManager
) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val inbuiltFragment = InbuiltFragment.newInstance()
    private val thermalFragment = ThermalFragment.newInstance()

    private val TITLES = arrayOf(
        context.resources.getString(R.string.label_inbuilt),
        context.resources.getString(R.string.label_thermal)
    )

    override fun getItem(position: Int): BasePrinterFragment<*, *> {
        return when (position) {
            0 -> inbuiltFragment
            else -> thermalFragment
        }
    }

    override fun getPageTitle(position: Int): CharSequence {
        return TITLES[position]
    }

    /*override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }*/

    override fun getCount(): Int {
        return TITLES.size
    }
}