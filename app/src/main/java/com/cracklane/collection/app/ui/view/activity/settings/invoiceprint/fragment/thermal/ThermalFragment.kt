package com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.thermal

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.session.ISession
import com.cracklane.collection.app.databinding.FragmentThermalBinding
import com.cracklane.collection.app.ui.view.activity.print.BluetoothHelper
import com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.InvoicePrintSettingsActivity
import com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.BasePrinterFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ThermalFragment : BasePrinterFragment<ThermalViewModel, FragmentThermalBinding>() {

    override val viewModel: ThermalViewModel by viewModels()
    override val layoutRes = R.layout.fragment_thermal

    private var thermalPrinterAdapter: ThermalPrinterAdapter? = null

    @Inject
    lateinit var session: ISession

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(viewModel.btHelper)
        thermalPrinterAdapter = ThermalPrinterAdapter(session) {
            (requireActivity() as InvoicePrintSettingsActivity).notifyPrinterChanged()
        }
        dataBinding.recyclerview.run {
            layoutManager = LinearLayoutManager(activity)
            adapter = thermalPrinterAdapter
        }

        viewModel.printerList.observe(viewLifecycleOwner) {
            it.getContentIfNotHandled()?.let {
                thermalPrinterAdapter?.setPrinterList(it)
            }
        }
    }

    override fun notifyPrinterChanged() {
        thermalPrinterAdapter?.refreshSelectedPrinter()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ThermalFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}