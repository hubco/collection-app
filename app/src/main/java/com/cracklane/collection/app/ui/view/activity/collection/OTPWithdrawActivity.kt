package com.cracklane.collection.app.ui.view.activity.collection

import android.app.AlertDialog
import android.os.Bundle
import androidx.activity.viewModels
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.CollectionType
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.common.extensions.dialog.errorAlert
import com.cracklane.collection.app.common.extensions.dialog.otpAlert
import com.cracklane.collection.app.common.extensions.dialog.printAlert
import com.cracklane.collection.app.common.put
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.SearchAccountData
import com.cracklane.collection.app.databinding.ActivityOtpwithdrawBinding
import com.cracklane.collection.app.ui.navigation.navigateForResult
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import com.cracklane.collection.app.ui.view.activity.search.SearchAccountActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OTPWithdrawActivity : BaseActivity<ActivityOtpwithdrawBinding>() {

    override val viewModel: OTPWithdrawViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_otpwithdraw

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    private var otpAlertDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.tvAccountType.setOnClickListener {
            binding.spnAccountType.performClick()
        }

        val collectionType = CollectionType.values()[intent.getIntExtra(
            Constants.EXTRA_COLLECTION_TYPE,
            CollectionType.OTP_WITHDRAWAL.ordinal,
        )]

        viewModel.setCollectionType(collectionType)
    }

    override fun registerObserver() {
        super.registerObserver()
        viewModel.selectAccountClickEvent.observe(this) {
            viewModel.selectedAccountCode.let { code ->
                navigateForResult<SearchAccountActivity, SearchAccountData>(Bundle().apply {
                    put(Constants.EXTRA_ACCOUNT_TYPE, code)
                }) { success, data ->
                    if (success) {
                        viewModel.setAccountSearchResult(data)
                    }
                }
            }
        }

        viewModel.fetchAccountInfoFailureEvent.observe(this) {
            errorAlert(it)
        }

        viewModel.otpRequestResultData.observe(this) {
            hideProgressDialog()
            when (it) {
                is Result.Success -> {
                    otpAlertDialog =
                        otpAlert(it.data.message, "") { isResendOtp, otpCode ->
                            when (isResendOtp) {
                                true -> {
                                    otpAlertDialog?.dismiss()
                                    showProgressDialog()
                                    viewModel.resendOtp(it.data.otpId)
                                }
                                false -> {
                                    showProgressDialog()
                                    viewModel.verifyOtp(it.data.otpId, otpCode)
                                }
                            }
                        }
                }
                is Result.Error -> {
                    otpAlertDialog?.dismiss()
                    errorAlert(it.error)
                }
            }
        }

        viewModel.verifyOtpResultData.observe(this) {
            otpAlertDialog?.dismiss()
            hideProgressDialog()
            when (it) {
                is Result.Success -> {
                    printAlert(
                        "Collection submitted!"
                    )
                }
                is Result.Error -> {
                    errorAlert(it.error)
                }
            }
        }
    }
}