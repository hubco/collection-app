package com.cracklane.collection.app.ui.view.activity.transaction

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cracklane.collection.app.data.remote.response.SearchAccountData
import com.cracklane.collection.app.data.remote.response.TransactionData
import com.cracklane.collection.app.databinding.LayoutSearchItemBinding
import com.cracklane.collection.app.databinding.LayoutTransactionBinding

class TransactionListAdapter(private val list: List<TransactionData>,
                             private val callback: ((data: TransactionData) -> Unit)? = null) :
    RecyclerView.Adapter<TransactionListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TransactionListAdapter.ViewHolder {
        val binding = LayoutTransactionBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: TransactionListAdapter.ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class ViewHolder(val binding: LayoutTransactionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: TransactionData) {
            binding.memberInfo = "${data.acNo} - ${data.memberName} - ${data.memberNo}"
            binding.date = data.openDate
            binding.amount = "Amount: ${data.amount}"
            binding.root.setOnClickListener {
                callback?.invoke(data)
            }
        }
    }
}