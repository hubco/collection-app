package com.cracklane.collection.app.common.session

import androidx.lifecycle.Observer
import com.cracklane.collection.app.data.remote.response.LoginResponse

interface ISession {

    fun saveUser(user: LoginResponse)
    val isLoggedIn: Boolean
    val isFirstLaunch: Boolean
    val userId: String
    val userToken: String
    val userName: String
    val userImage: String
    val user: LoginResponse
    var isBiometricEnabled: Boolean
    var setBiometricLogin: Boolean
    var printerCode: String
    var printerName: String
    var isInbuiltPriterSelected: Boolean
    fun logout()
    fun clearSession()
}