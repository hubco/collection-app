package com.cracklane.collection.app.domain.repository

import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.BankAccounts
import com.cracklane.collection.app.data.remote.response.NameValuePair
import com.cracklane.collection.app.data.remote.response.SettingsResponse

interface ISettingsRepository {

    suspend fun loadSettings(): Result<SettingsResponse>

    suspend fun getAccounts(): List<NameValuePair>?

    suspend fun getAccountsOtp(): List<NameValuePair>?

    suspend fun gettTypes(): List<NameValuePair>?

    suspend fun getCompanyName(): String?

    suspend fun getCinNo() : String?

    suspend fun getPaymentModes() : List<String>?

    suspend fun getBankAccounts() : List<BankAccounts>?

    suspend fun getBankList() : List<String>?

    suspend fun getTransferModes() : List<NameValuePair>?
}