package com.cracklane.collection.app.ui.view.activity.password

import android.os.Bundle
import androidx.activity.viewModels
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.toast
import com.cracklane.collection.app.databinding.ActivityChangePasswordBinding
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChangePasswordActivity : BaseActivity<ActivityChangePasswordBinding>() {

    override val viewModel: ChangePasswordViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_change_password

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.passwordChangeSuccessfulEvent.observe(this) {
            if(it != null) {
                toast(it)
            } else {
                toast(R.string.label_password_changed)
            }
            finish()
        }

        viewModel.passwordChangeFailureEvent.observe(this) {
            toast(it)
        }
    }
}