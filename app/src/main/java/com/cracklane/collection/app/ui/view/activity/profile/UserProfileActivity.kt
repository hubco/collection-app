package com.cracklane.collection.app.ui.view.activity.profile

import androidx.activity.viewModels
import com.cracklane.collection.app.R
import com.cracklane.collection.app.databinding.ActivityUserProfileBinding
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserProfileActivity : BaseActivity<ActivityUserProfileBinding>() {

    override val viewModel: UserProfileViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_user_profile

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true
}