package com.cracklane.collection.app.di

import com.cracklane.collection.app.data.remote.service.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    fun provideAuthService(builder: Retrofit.Builder): AuthService =
        builder.build().create(AuthService::class.java)

    @Provides
    fun provideSettingsService(builder: Retrofit.Builder): SettingsService =
        builder.build().create(SettingsService::class.java)

    @Provides
    fun provideAccountService(builder: Retrofit.Builder): AccountService =
        builder.build().create(AccountService::class.java)

    @Provides
    fun provideUserService(builder: Retrofit.Builder): UserService =
        builder.build().create(UserService::class.java)

    @Provides
    fun provideEnquiryervice(builder: Retrofit.Builder): EnquiryService =
        builder.build().create(EnquiryService::class.java)
}