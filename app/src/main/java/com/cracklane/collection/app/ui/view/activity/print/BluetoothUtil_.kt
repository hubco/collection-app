package com.cracklane.collection.app.ui.view.activity.print

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.widget.Toast
import com.cracklane.collection.app.common.extensions.dialog.bluetoothConfirmationAlert
import java.io.IOException
import java.io.OutputStream
import java.util.*

object BluetoothUtil_ {
    private val PRINTER_UUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
    private val Innerprinter_Address = "00:11:22:33:44:55"
    var isBlueToothPrinter = false
    private var bluetoothSocket: BluetoothSocket? = null

    private fun getBTAdapter(context: Context?): BluetoothAdapter? {
        val bluetoothManager = context?.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        return bluetoothManager.adapter
    }

    fun enableBluetooth(context: Context?){
        if(getBTAdapter(context) == null){
            Toast.makeText(context, "Bluetooth Not Supported", Toast.LENGTH_SHORT).show()
            return
        }
            context?.bluetoothConfirmationAlert {
                getBTAdapter(context)?.enable()
        }
    }

    fun isBluetoothEnabled(context: Context?) : Boolean?{
        if(getBTAdapter(context) == null){
            Toast.makeText(context, "Bluetooth Not Supported", Toast.LENGTH_SHORT).show()
            return false
        }
        return getBTAdapter(context)?.isEnabled
    }

    fun disableBluetooth(context: Context?){
        if(getBTAdapter(context) == null){
            Toast.makeText(context, "Bluetooth Not Supported", Toast.LENGTH_SHORT).show()
            return
        }
            context?.bluetoothConfirmationAlert {
                getBTAdapter(context)?.disable()
        }
    }

    fun getPairedDevices(context: Context?) : List<BluetoothDevice>?{
        if(getBTAdapter(context) == null){
            Toast.makeText(context, "Bluetooth Not Supported", Toast.LENGTH_SHORT).show()
            return null
        }
       return getBTAdapter(context)?.bondedDevices?.toList()

    }

    private fun getDevice(bluetoothAdapter: BluetoothAdapter): BluetoothDevice? {
        var innerprinter_device: BluetoothDevice? = null
        val devices = bluetoothAdapter.bondedDevices
        for (device in devices) {
            if (device.address == Innerprinter_Address) {
                innerprinter_device = device
                break
            }
        }
        return innerprinter_device
    }

    @Throws(IOException::class)
    private fun getSocket(device: BluetoothDevice): BluetoothSocket? {
        val socket: BluetoothSocket = device.createRfcommSocketToServiceRecord(PRINTER_UUID)
        socket.connect()
        return socket
    }

    /**
     * connect bluetooth
     */
    fun connectBlueTooth(context: Context?): Boolean {
        if (bluetoothSocket == null) {
            if (getBTAdapter(context) == null) {
//                Toast.makeText(context, R.string.toast_3, Toast.LENGTH_SHORT).show()
                return false
            }
            if (!getBTAdapter(context)!!.isEnabled) {
//                Toast.makeText(context, R.string.toast_4, Toast.LENGTH_SHORT).show()
                return false
            }
            var device: BluetoothDevice?
            if (getDevice(getBTAdapter(context)!!).also { device = it } == null) {
//                Toast.makeText(context, R.string.toast_5, Toast.LENGTH_SHORT).show()
                return false
            }
            bluetoothSocket = try {
                getSocket(device!!)
            } catch (e: IOException) {
//                Toast.makeText(context, R.string.toast_6, Toast.LENGTH_SHORT).show()
                return false
            }
        }
        return true
    }

    /**
     * disconnect bluethooth
     */
    fun disconnectBlueTooth(context: Context?) {
        if (bluetoothSocket != null) {
            try {
                val out: OutputStream = bluetoothSocket!!.outputStream
                out.close()
                bluetoothSocket!!.close()
                bluetoothSocket = null
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * send esc cmd
     */
    fun sendData(bytes: ByteArray) {
        if (bluetoothSocket != null) {
            var out: OutputStream? = null
            try {
                out = bluetoothSocket!!.outputStream
                out.write(bytes, 0, bytes.size)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else {
            //TODO handle disconnect event
        }
    }
}