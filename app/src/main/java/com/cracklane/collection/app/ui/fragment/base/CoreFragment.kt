package com.cracklane.collection.app.ui.fragment.base

import androidx.fragment.app.Fragment

abstract class CoreFragment : Fragment()