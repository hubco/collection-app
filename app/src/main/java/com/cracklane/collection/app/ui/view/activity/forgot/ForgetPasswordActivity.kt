package com.cracklane.collection.app.ui.view.activity.forgot

import androidx.activity.viewModels
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.common.extensions.dialog.errorAlert
import com.cracklane.collection.app.data.remote.request.OtpVerificationRequest
import com.cracklane.collection.app.databinding.ActivityForgetPasswordBinding
import com.cracklane.collection.app.ui.navigation.Routes
import com.cracklane.collection.app.ui.navigation.navigateTo
import com.cracklane.collection.app.ui.navigation.navigateToLogin
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgetPasswordActivity : BaseActivity<ActivityForgetPasswordBinding>() {

    override val viewModel: ForgetPasswordViewModel by viewModels()
    override val layoutRes = R.layout.activity_forget_password

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun registerObserver() {
        super.registerObserver()
        viewModel.forgetPasswordSuccessEvent.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                navigateTo(
                    Routes.OTP_VERIFICATION_SCREEN,
                    mutableMapOf<String, OtpVerificationRequest>().apply {
                        put(Constants.EXTRA_OTP_VERIFICATION_DATA, it)
                    })
            }
        }
        viewModel.forgetPasswordFailureEvent.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                errorAlert(it)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        navigateToLogin()
    }
}