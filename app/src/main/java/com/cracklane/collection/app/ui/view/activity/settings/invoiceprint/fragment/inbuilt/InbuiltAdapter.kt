package com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.inbuilt

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cracklane.collection.app.common.session.ISession
import com.cracklane.collection.app.databinding.PrinterCardViewBinding
import javax.inject.Inject

class InbuiltAdapter @Inject constructor(
    var inbuiltList: List<InbuiltData>,
    val session: ISession,
    val callback: ()->Unit
) : RecyclerView.Adapter<InbuiltAdapter.InbuiltItemViewHolder>() {

    private var lastSelectedPosition = -1

    fun setPrinterList(inbuiltList: List<InbuiltData>) {
        this.inbuiltList = inbuiltList
        notifyDataSetChanged()
    }

    fun refreshSelectedPrinter() {
        lastSelectedPosition = -1
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InbuiltItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = PrinterCardViewBinding.inflate(layoutInflater, parent, false)
        return InbuiltItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InbuiltItemViewHolder, position: Int) {
        val inbuiltData = inbuiltList[position]
        holder.binding.printerName = inbuiltData.printerName
        holder.binding.executePendingBindings()

        if (lastSelectedPosition == -1) {
            holder.binding.switchView.isChecked = session.printerName == inbuiltData.printerName
        } else {
            holder.binding.switchView.isChecked = (lastSelectedPosition == position)
        }

        holder.binding.root.setOnClickListener {
            refreshData(holder, inbuiltData)
        }
        holder.binding.switchView.setOnClickListener {
            refreshData(holder, inbuiltData)
        }
    }

    private fun refreshData(
        holder: InbuiltItemViewHolder,
        inbuiltData: InbuiltData
    ) {
        lastSelectedPosition = holder.adapterPosition
        notifyDataSetChanged()
        session.printerName = inbuiltData.printerName!!
        session.printerCode = inbuiltData.printerCode!!
        session.isInbuiltPriterSelected = true
        callback()
    }

    override fun getItemCount(): Int = inbuiltList.size

    inner class InbuiltItemViewHolder(
        val binding: PrinterCardViewBinding,
    ) : RecyclerView.ViewHolder(binding.root)

}