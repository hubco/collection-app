package com.cracklane.collection.app.ui.navigation

import com.cracklane.collection.app.ui.view.activity.collection.CollectionActivity
import com.cracklane.collection.app.ui.view.activity.collection.OTPDepositActivity
import com.cracklane.collection.app.ui.view.activity.collection.OTPWithdrawActivity
import com.cracklane.collection.app.ui.view.activity.collection.list.CollectionListActivity
import com.cracklane.collection.app.ui.view.activity.dashboard.DashboardActivity
import com.cracklane.collection.app.ui.view.activity.enquiry.EnquiryActivity
import com.cracklane.collection.app.ui.view.activity.forgot.ForgetPasswordActivity
import com.cracklane.collection.app.ui.view.activity.login.LoginActivity
import com.cracklane.collection.app.ui.view.activity.otp.OtpVerificationActivity
import com.cracklane.collection.app.ui.view.activity.password.ChangePasswordActivity
import com.cracklane.collection.app.ui.view.activity.print.PrintActivity
import com.cracklane.collection.app.ui.view.activity.profile.UserProfileActivity
import com.cracklane.collection.app.ui.view.activity.settings.SettingsActivity
import com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.InvoicePrintSettingsActivity
import com.cracklane.collection.app.ui.view.activity.transaction.TransactionListActivity

object Routes {

    const val LOGIN_SCREEN = "LOGIN_SCREEN"
    const val FORGET_PASSWORD_SCREEN = "FORGET_PASSWORD_SCREEN"
    const val OTP_VERIFICATION_SCREEN = "OTP_VERIFICATION_SCREEN"
    const val DASHBOARD_SCREEN = "DASHBOARD_SCREEN"
    const val COLLECTION_SCREEN = "COLLECTION_SCREEN"
    const val OTP_DEPOSIT_SCREEN = "OTP_DEPOSIT_SCREEN"
    const val OTP_WITHDRAW_SCREEN = "OTP_WITHDRAW_SCREEN"
    const val SETTINGS_SCREEN = "SETTINGS_SCREEN"
    const val CHANGE_PASSWORD_SCREEN = "CHANGE_PASSWORD_SCREEN"
    const val USER_PROFILE_SCREEN = "USER_PROFILE_SCREEN"
    const val INVOICE_PRINT_SETTINGS = "INVOICE_PRINT_SETTINGS"
    const val PRINT_COLLECTION_SCREEN = "PRINT_COLLECTION_SCREEN"
    const val TRANSACTION_LIST_SCREEN = "TRANSACTION_LIST_SCREEN"
    const val COLLECTION_LIST_SCREEN = "COLLECTION_LIST_SCREEN"
    const val ENQUIRY_SCREEN = "ENQUIRY_SCREEN"

    val navigationMap: Map<String, Class<*>> = HashMap<String, Class<*>>().apply {
        put(LOGIN_SCREEN, LoginActivity::class.java)
        put(FORGET_PASSWORD_SCREEN, ForgetPasswordActivity::class.java)
        put(OTP_VERIFICATION_SCREEN, OtpVerificationActivity::class.java)
        put(DASHBOARD_SCREEN, DashboardActivity::class.java)
        put(COLLECTION_SCREEN, CollectionActivity::class.java)
        put(OTP_DEPOSIT_SCREEN, OTPDepositActivity::class.java)
        put(OTP_WITHDRAW_SCREEN, OTPWithdrawActivity::class.java)
        put(SETTINGS_SCREEN, SettingsActivity::class.java)
        put(CHANGE_PASSWORD_SCREEN, ChangePasswordActivity::class.java)
        put(USER_PROFILE_SCREEN, UserProfileActivity::class.java)
        put(INVOICE_PRINT_SETTINGS, InvoicePrintSettingsActivity::class.java)
        put(PRINT_COLLECTION_SCREEN, PrintActivity::class.java)
        put(TRANSACTION_LIST_SCREEN, TransactionListActivity::class.java)
        put(COLLECTION_LIST_SCREEN, CollectionListActivity::class.java)
        put(ENQUIRY_SCREEN, EnquiryActivity::class.java)
    }
}