package com.cracklane.collection.app.data.remote.response

data class GenericResponse(
    val message: String?
)