package com.cracklane.collection.app.data.remote.response

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class SettingsResponse(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val companyName: String,
    val cinNo: String?,
    val accountTypes: List<List<String>>,
    val accountTypesOtp: List<List<String>>,
    val tTypes: List<List<String>>,
    val printerTypes: List<String>,
    val activeModules: ActiveModules,
    val paymentModes: List<String>,
    val bankAccounts: List<BankAccounts>,
    var bankList: List<String>?,
    val transferModes: List<List<String>>
) {
    val accounts: List<NameValuePair>
        get() = accountTypes.map { NameValuePair(it[1], it[0]) }

    val accountsOtp: List<NameValuePair>
        get() = accountTypesOtp.map { NameValuePair(it[1], it[0]) }

    val tType: List<NameValuePair>
        get() = tTypes.map { NameValuePair(it[1], it[0]) }

    val transferModeList: List<NameValuePair>
        get() = transferModes.map { NameValuePair(it[0], it[1]) }
}

data class NameValuePair(
    val name: String,
    val value: String
)

data class ActiveModules(
    @SerializedName("cash_deposit")
    val isCashDepositEnabled: Boolean,
    @SerializedName("cash_deposit_otp")
    val isCashDepositOtpEnabled: Boolean,
    @SerializedName("cash_withdrawal_otp")
    val isCashWithdrawalOtpEnabled: Boolean,
    @SerializedName("inquiry_form")
    val isEnquiryFormEnabled: Boolean
)

data class BankAccounts(
    @SerializedName("name")
    val bankName: String,
    @SerializedName("bank_account_id")
    val bankId: String
)

fun String.toSimplifiedPaymentMode(): String {
    var delegatePaymentMode = this
    delegatePaymentMode = delegatePaymentMode.replace("_", " ")
    if (delegatePaymentMode.first().isLowerCase())
        delegatePaymentMode = delegatePaymentMode.replaceFirstChar {
            if (it.isLowerCase()) it.titlecase(
                Locale.getDefault()
            ) else it.toString()
        }
    if (delegatePaymentMode.contains(" ")) {
        val words = delegatePaymentMode.split(" ")
        delegatePaymentMode = ""
        for (word in words) {
            delegatePaymentMode += word.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
        }
    }
    return delegatePaymentMode
}