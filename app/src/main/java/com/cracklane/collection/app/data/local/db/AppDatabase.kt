package com.cracklane.collection.app.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.cracklane.collection.app.data.local.db.dao.AppDao
import com.cracklane.collection.app.data.remote.response.SettingsResponse

const val DB_NAME = "collection_app_db"

@Database(entities = [SettingsResponse::class], version = 5)
@TypeConverters(Converter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun appDao(): AppDao
}