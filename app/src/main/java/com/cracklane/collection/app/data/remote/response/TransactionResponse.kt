package com.cracklane.collection.app.data.remote.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class TransactionResponse(
    val response: List<TransactionData>,
    val status: String
)

@Parcelize
data class TransactionData(
    val acBalance: String,
    val acNo: String,
    val acSlug: String,
    val acType: String,
    val amount: String,
    val memberName: String,
    val memberNo: String,
    val mobileNo: String,
    val openDate: String,
    val pMode: String,
    val remarks: String?,
    val scheme: String,
    val tDate: String,
    val tId: String,
    val tStatus: String,
    val tType: String,
    val isPrint: Boolean
) : Parcelable