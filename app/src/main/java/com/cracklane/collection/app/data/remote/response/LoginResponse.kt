package com.cracklane.collection.app.data.remote.response

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class LoginResponse(
    @PrimaryKey
    val userId: String,
    val type: String,
    val token: String,
    val name: String,
    val imageUrl: String?
)
