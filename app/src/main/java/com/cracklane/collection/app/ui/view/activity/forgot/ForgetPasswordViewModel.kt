package com.cracklane.collection.app.ui.view.activity.forgot

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.data.dataclass.Event
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.request.OtpVerificationRequest
import com.cracklane.collection.app.data.remote.response.toOtpVerificationRequest
import com.cracklane.collection.app.domain.repository.IAuthRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ForgetPasswordViewModel @Inject constructor(private val repository: IAuthRepository) :
    BaseViewModel() {

    val userNameData = MutableLiveData<String>()
    val submitButtonEnabled = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(userNameData) {
            value = !userNameData.value.isNullOrEmpty()
        }
    }

    private val _forgetPasswordSuccessEvent = MutableLiveData<Event<OtpVerificationRequest>>()
    val forgetPasswordSuccessEvent: LiveData<Event<OtpVerificationRequest>> =
        _forgetPasswordSuccessEvent

    private val _forgetPasswordFailureEvent = MutableLiveData<Event<String>>()
    val forgetPasswordFailureEvent: LiveData<Event<String>> = _forgetPasswordFailureEvent

    fun onSubmitButtonClick() {
        viewModelScope.launch {
            loading(true)
            when (val result = repository.forgetPassword(userNameData.value!!)) {
                is Result.Success -> _forgetPasswordSuccessEvent.value =
                    Event(result.data.toOtpVerificationRequest(userNameData.value!!))
                is Result.Error -> _forgetPasswordFailureEvent.value = Event(result.error)
            }
            loading(false)
        }
    }

    override fun onCleared() {
        super.onCleared()
        submitButtonEnabled.removeSource(userNameData)
    }
}