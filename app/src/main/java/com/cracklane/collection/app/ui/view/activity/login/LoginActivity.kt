package com.cracklane.collection.app.ui.view.activity.login

import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.cracklane.collection.app.ui.navigation.navigateTo
import com.cracklane.collection.app.ui.navigation.navigateToDashboard
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.Biometric
import com.cracklane.collection.app.common.extensions.dialog.errorAlert
import com.cracklane.collection.app.databinding.ActivityLoginBinding
import com.cracklane.collection.app.ui.navigation.Routes
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import com.sunmi.peripheral.printer.SunmiPrinterService
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : BaseActivity<ActivityLoginBinding>() {

    override val viewModel: LoginViewModel by viewModels()
    override val layoutRes = R.layout.activity_login

    lateinit var service: SunmiPrinterService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // update view visibility as per gradle configuration
        binding.llBottom.isVisible = true
    }

    override fun registerObserver() {
        super.registerObserver()
        viewModel.forgotClickEvent.observe(this, {
            navigateTo(Routes.FORGET_PASSWORD_SCREEN)
        })

        viewModel.quickLoginClickEvent.observe(this, {
            Biometric.authenticate(this) { success, error ->
                if (success) {
                    viewModel.authenticateQuickLogin()
                } else {
                    errorAlert(
                        error?.toString() ?: getString(R.string.biometric_authentication_error)
                    )
                }
            }
        })

        viewModel.loginSuccessEvent.observe(this, {
            navigateToDashboard()
        })

        viewModel.loginFailureEvent.observe(this, { event ->
            event.getContentIfNotHandled()?.let {
                errorAlert(it)
            }
        })
    }
}