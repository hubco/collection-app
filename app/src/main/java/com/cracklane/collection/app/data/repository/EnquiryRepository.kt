package com.cracklane.collection.app.data.repository

import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.request.OtpVerificationRequest
import com.cracklane.collection.app.data.remote.response.GenericResponse
import com.cracklane.collection.app.data.remote.service.EnquiryService
import com.cracklane.collection.app.domain.repository.IEnquiryRepository
import javax.inject.Inject

class EnquiryRepository @Inject constructor(private val service: EnquiryService) :
    BaseRepository(), IEnquiryRepository {

    override suspend fun postEntry(
        inquiryType: String,
        firstName: String,
        lastName: String,
        aadharNo: String,
        panNo: String,
        address: String,
        mobNo: String,
        dob: String,
        remarks: String,
        photo: String?
    ): Result<OtpVerificationRequest> {
        return makeNetworkCall {
            service.postEnquiry(
                inquiryType,
                firstName,
                lastName,
                aadharNo,
                panNo,
                address,
                mobNo,
                dob,
                remarks,
                photo
            )
        }
    }

    override suspend fun verifyOtp(otpId: String, otpCode: String): Result<GenericResponse> {
        return makeNetworkCall { service.verifyOtp(otpId, otpCode) }
    }
}