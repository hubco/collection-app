package com.cracklane.collection.app.ui.view.activity.settings

import androidx.activity.viewModels
import com.cracklane.collection.app.R
import com.cracklane.collection.app.databinding.ActivitySettingsBinding
import com.cracklane.collection.app.ui.navigation.Routes
import com.cracklane.collection.app.ui.navigation.navigateTo
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingsActivity : BaseActivity<ActivitySettingsBinding>() {

    override val viewModel: SettingsViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_settings

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun registerObserver() {
        super.registerObserver()
        viewModel.invoicePrintClickEvent.observe(this) {
            navigateTo(Routes.INVOICE_PRINT_SETTINGS)
        }
    }
}