package com.cracklane.collection.app.common

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.lifecycle.*
import com.cracklane.collection.app.R
import java.io.*
import java.util.*
import android.graphics.Bitmap
import android.util.Base64
import androidx.annotation.WorkerThread


fun Map<String, Any?>.toBundle(): Bundle =
    Bundle().apply { this@toBundle.forEach { put(it.key, it.value) } }

fun Context.showToast(@StringRes id: Int?, duration: Int = Toast.LENGTH_SHORT) {
    id?.let {
        showToast(getString(it), duration)
    }
}

fun Context.showToast(text: String?, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, text ?: "", duration).show()
}

fun Bundle.put(key: String, value: Any?): Bundle {
    when (value) {
        is Boolean -> putBoolean(key, value)
        is BooleanArray -> putBooleanArray(key, value)
        is Bundle -> putBundle(key, value)
        is Byte -> putByte(key, value)
        is ByteArray -> putByteArray(key, value)
        is Char -> putChar(key, value)
        is CharArray -> putCharArray(key, value)
        is CharSequence -> putCharSequence(key, value)
        is Double -> putDouble(key, value)
        is DoubleArray -> putDoubleArray(key, value)
        is Float -> putFloat(key, value)
        is FloatArray -> putFloatArray(key, value)
        is Int -> putInt(key, value)
        is IntArray -> putIntArray(key, value)
        is Long -> putLong(key, value)
        is LongArray -> putLongArray(key, value)
        is Parcelable -> putParcelable(key, value)
        is Serializable -> putSerializable(key, value)
        is Short -> putShort(key, value)
        is ShortArray -> putShortArray(key, value)
        is String -> putString(key, value)
        else -> throw IllegalArgumentException("Illegal type of value argument!")
    }
    return this
}

/**
 * Show hidden view
 */
fun View.showView() {
    if (!this.isShown) this.visibility = View.VISIBLE
}

/**
 * Hide View
 */
fun View.hideView() {
    if (this.isShown) this.visibility = View.GONE
}

/**
 * Show multiple views
 */
fun showViews(vararg views: View) {
    views.forEach { view -> view.showView() }
}

/**
 * Hide multiple views
 */
fun hideViews(vararg views: View) {
    views.forEach { view -> view.hideView() }
}

/**
 * Extension method to show a keyboard for View.
 */
fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.requestFocus()
    imm.showSoftInput(this, 0)
}

/**
 * Try to hide the keyboard and returns whether it worked
 * https://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
 */
fun View.hideKeyboard(): Boolean {
    try {
        val inputMethodManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

/**
 * Try to hide the keyboard and returns whether it worked
 * https://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
 */
fun Activity.hideKeyboard(): Boolean {
    try {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

/**
 * Extension method to show toast for Context.
 */
fun Context.toast(text: CharSequence, duration: Int = Toast.LENGTH_LONG) =
    Toast.makeText(this, text, duration).show()

fun Context.toDp(value: Float) =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, resources.displayMetrics)

fun String.removeSpecialChars(): String {
    return Regex("[^a-zA-Z0-9]").replace(this, "")
}

fun Context.toast(message: CharSequence) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.toastInCenter(message: CharSequence) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).apply {
        setGravity(Gravity.CENTER, 0, 0)
    }.show()
}

fun Context.toastInCenter(textId: Int) {
    Toast.makeText(this, getString(textId), Toast.LENGTH_SHORT).apply {
        setGravity(Gravity.CENTER, 0, 0)
    }.show()
}

fun Context.toast(textId: Int) {
    Toast.makeText(this, getString(textId), Toast.LENGTH_SHORT).show()
}

@SuppressLint("ClickableViewAccessibility")
fun EditText.setupClearButtonWithAction() {

    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            val clearIcon = if (editable?.isNotEmpty() == true) R.drawable.ic_close else 0
            setCompoundDrawablesWithIntrinsicBounds(0, 0, clearIcon, 0)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
    })

    setOnTouchListener(View.OnTouchListener { _, event ->
        if (event.action == MotionEvent.ACTION_UP) {
            if (event.rawX >= (this.right - this.compoundPaddingRight)) {
                this.setText("")
                return@OnTouchListener true
            }
        }
        return@OnTouchListener false
    })
}

@WorkerThread
fun Bitmap.toBase64(): String? {
    val baos = ByteArrayOutputStream()
    compress(Bitmap.CompressFormat.JPEG, 70, baos)
    return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT)
}