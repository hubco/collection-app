package com.cracklane.collection.app.ui.view.activity.print

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.common.extensions.dialog.errorAlert
import com.cracklane.collection.app.databinding.ActivityPrintBinding
import com.cracklane.collection.app.ui.navigation.Routes
import com.cracklane.collection.app.ui.navigation.navigateTo
import com.cracklane.collection.app.ui.view.activity.async.AsyncBluetoothEscPosPrint
import com.cracklane.collection.app.ui.view.activity.async.AsyncEscPosPrinter
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import com.dantsu.escposprinter.connection.DeviceConnection
import com.dantsu.escposprinter.connection.bluetooth.BluetoothConnection
import com.dantsu.escposprinter.connection.bluetooth.BluetoothPrintersConnections
import com.dantsu.escposprinter.textparser.PrinterTextParserImg
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_print.*

@AndroidEntryPoint
class PrintActivity : BaseActivity<ActivityPrintBinding>() {

    override val viewModel: PrintViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_print

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true
    private var bitmap: Bitmap? = null

    companion object {
        @JvmStatic
        val PERMISSION_BLUETOOTH = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent.getBooleanExtra(Constants.EXTRA_FROM_TRANSACTION_SCREEN, false)) {
            viewModel.setTransactionData(intent.getParcelableExtra(Constants.EXTRA_TRANSACTION_DATA))
        } else {
            viewModel.setCollectionResult(intent.getParcelableExtra(Constants.EXTRA_COLLECTION_RESPONSE))
            viewModel.setAccountResult(intent.getParcelableExtra(Constants.EXTRA_ACCOUNT_INFO))
            viewModel.amount.value = intent.getStringExtra("amount")
            viewModel.remarks.value = intent.getStringExtra("remarks")
        }
    }

    override fun registerObserver() {
        super.registerObserver()
        viewModel.printClickEvent.observe(this) {
            if (session.isInbuiltPriterSelected) {
                bitmap = createViewBitmap(bill_layout)
                SunmiPrintHelper.getInstance()?.printBitmap(bitmap, 0)
                SunmiPrintHelper.getInstance()?.feedPaper()
            } else {
                if (session.printerName.isEmpty()) {
                    errorAlert(getString(R.string.no_printer_selected)) {
                        navigateTo(Routes.SETTINGS_SCREEN)
                    }
                } else {
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.BLUETOOTH
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(Manifest.permission.BLUETOOTH),
                            PERMISSION_BLUETOOTH
                        )
                    } else {
                        // get select bluetooth device
                        val bluetoothDevicesList = BluetoothPrintersConnections().list
                        var selectedDevice: BluetoothConnection? = null
                        if (bluetoothDevicesList != null) {
                            for (device in bluetoothDevicesList) {
                                if (device.device.name == session.printerName) {
                                    selectedDevice = device
                                }
                            }
                            AsyncBluetoothEscPosPrint(this).execute(
                                getAsyncEscPosPrinter(
                                    selectedDevice
                                )
                            )
                        } else {
                            errorAlert(getString(R.string.no_bluetooth_printer_found))
                        }
                    }
                }
            }
        }
    }

    private fun createViewBitmap(v: View): Bitmap {
        v.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        val bitmap =
            Bitmap.createBitmap(
                v.measuredWidth,
                v.measuredHeight,
                Bitmap.Config.ARGB_8888
            )
        val canvas = Canvas(bitmap)
        canvas.drawColor(Color.WHITE)
        v.layout(0, 0, v.width, v.height)

        v.draw(canvas)
        return bitmap
    }

    @SuppressLint("SimpleDateFormat")
    private fun getAsyncEscPosPrinter(printerConnection: DeviceConnection?): AsyncEscPosPrinter? {
        val companyName = viewModel.memberCompanyName.value
        val cinNO = viewModel.memberCinNo.value
        val regNo = viewModel.memberRegNo.value
        val name = viewModel.memberName.value
        val acNo = viewModel.memberAcNo.value
        val phone = viewModel.memberPhone.value
        val date = viewModel.date.value
        val refId = viewModel.refId.value
        val amount = viewModel.amount.value
        val mode = viewModel.mode.value
        val status = viewModel.status.value
        val avBal = viewModel.availBal.value
        val remarks = viewModel.remarks.value
        val printDate = viewModel.printedOn.value
        val printBy = viewModel.printedBy.value
        val printer = AsyncEscPosPrinter(printerConnection, 203, 48f, 32)
        return printer.setTextToPrint(
            """
                [C]<img>${
                PrinterTextParserImg.bitmapToHexadecimalString(
                    printer,
                    this.applicationContext.resources.getDrawableForDensity(
                        R.drawable.ic_logo,
                        DisplayMetrics.DENSITY_MEDIUM
                    )
                )
            }</img>
                [L]
                [C]<u><font size='medium'>${companyName}</font></u>
                [L]
                [C]<u type='double'>${cinNO}</u>
                [C]================================
                [L]<b>Reg. no. :</b>[R]${regNo}
                [L]<b>Name :</b>[R]${name}
                [L]<b>A/c No. :</b>[R]${acNo}
                [L]<b>Phone no. :</b>[R]${phone}
                [C]================================
                [L]<b>Date :</b>[R]${date}
                [L]<b>Ref. ID :</b>[R]${refId}
                [L]<b>Amount :</b>[R]${amount}
                [L]<b>Mode :</b>[R]${mode}
                [L]<b>Status :</b>[R]${status}
                [L]<b>Avail Bal :</b>[R]${avBal}
                [L]<b>Remarks :</b>[R]${remarks}
                [C]================================
                [C]<b>Printed On :</b>[R]${printDate} by $printBy
                [C]<b>Thank you For your Business</b>
                [C]================================
                """.trimIndent()

        )
    }
}