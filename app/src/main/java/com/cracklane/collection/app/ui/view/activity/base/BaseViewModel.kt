package com.cracklane.collection.app.ui.view.activity.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {

    val isLoading = MutableLiveData(false)

    protected fun loading(value: Boolean) {
        isLoading.value = value
    }
}