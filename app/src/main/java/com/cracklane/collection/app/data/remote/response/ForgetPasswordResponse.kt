package com.cracklane.collection.app.data.remote.response

import android.os.Parcelable
import com.cracklane.collection.app.data.remote.request.OtpVerificationRequest
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ForgetPasswordResponse(
    val otpId: String,
    val message: String?
) : Parcelable

fun ForgetPasswordResponse.toOtpVerificationRequest(userName: String) =
    OtpVerificationRequest(userName = userName, otpId = otpId, message = message)