package com.cracklane.collection.app.ui.view.activity.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.SearchAccountData
import com.cracklane.collection.app.domain.repository.IAccountRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchAccountViewModel @Inject constructor(
    private val accountRepository: IAccountRepository
) : BaseViewModel() {

    private val _showHint = MutableLiveData(true)
    val showHint: LiveData<Boolean> = _showHint

    private val _searchResult = MutableLiveData<List<SearchAccountData>>()
    val searchResult: LiveData<List<SearchAccountData>> = _searchResult

    private val _noRecordFound = MediatorLiveData<Boolean>().apply {
        addSource(_searchResult) {
            value = it.isNullOrEmpty()
        }
    }
    val noRecordFound: LiveData<Boolean> = _noRecordFound

    lateinit var accountType: String
    private var _searchJob: Job? = null

    fun queryData(query: CharSequence) {
        val startSearch = query.length >= 3
        _showHint.value = !startSearch
        loading(startSearch)
        if (startSearch) {
            // perform search
            _searchJob = viewModelScope.launch {
                when (val result = accountRepository.searchAccount(accountType, query.toString())) {
                    is Result.Success -> _searchResult.value = result.data.results
                    is Result.Error -> _noRecordFound.value = _searchResult.value?.isEmpty()
                }
                loading(false)
            }
        } else {
            _searchJob?.cancel()
            _searchResult.value = emptyList()
        }
    }

    override fun onCleared() {
        super.onCleared()
        _noRecordFound.removeSource(_searchResult)
    }
}