package com.cracklane.collection.app.data.remote.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AccountInfoResponse(
    val acBalance: String?,
    val acNo: String,
    val acSlug: String,
    val amountToCollect: String,
    val memberName: String,
    val memberNo: String,
    val mobileNo: String,
    val openDate: String,
    val scheme: String,
) : Parcelable