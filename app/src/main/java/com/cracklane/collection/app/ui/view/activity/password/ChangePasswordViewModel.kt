package com.cracklane.collection.app.ui.view.activity.password

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.common.architecture.SingleLiveEvent
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.domain.repository.IAuthRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChangePasswordViewModel @Inject constructor(
    private val repository: IAuthRepository
) : BaseViewModel() {

    val passwordChangeSuccessfulEvent = SingleLiveEvent<String?>()
    val passwordChangeFailureEvent = SingleLiveEvent<String>()

    val oldPassword = MutableLiveData<String>()
    val newPassword = MutableLiveData<String>()
    val confirmPassword = MutableLiveData<String>()

    val isButtonEnabled = MediatorLiveData<Boolean>().apply {
        addSource(oldPassword) {
            value = !oldPassword.value.isNullOrEmpty()
                    && !newPassword.value.isNullOrEmpty()
                    && !confirmPassword.value.isNullOrEmpty()
        }
        addSource(newPassword) {
            value = !oldPassword.value.isNullOrEmpty()
                    && !newPassword.value.isNullOrEmpty()
                    && !confirmPassword.value.isNullOrEmpty()
        }
        addSource(confirmPassword) {
            value = !oldPassword.value.isNullOrEmpty()
                    && !newPassword.value.isNullOrEmpty()
                    && !confirmPassword.value.isNullOrEmpty()
        }
    }

    fun onChangePasswordClicked() {
        viewModelScope.launch {
            loading(true)
            when (val result =
                repository.changePassword(oldPassword.value!!, newPassword.value!!)) {
                is Result.Success -> passwordChangeSuccessfulEvent.value = result.data.message
                is Result.Error -> passwordChangeFailureEvent.value = result.error
            }
            loading(false)
        }
    }

    override fun onCleared() {
        super.onCleared()
        isButtonEnabled.removeSource(oldPassword);
        isButtonEnabled.removeSource(newPassword);
        isButtonEnabled.removeSource(confirmPassword);
    }

}