package com.cracklane.collection.app.ui.view.activity.splash

import android.annotation.SuppressLint
import android.os.Bundle
import com.cracklane.collection.app.R
import com.cracklane.collection.app.ui.navigation.Routes
import com.cracklane.collection.app.ui.navigation.navigateToAndFinish
import com.cracklane.collection.app.ui.view.activity.base.CoreActivity
import dagger.hilt.android.AndroidEntryPoint

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashActivity : CoreActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_CollectionApp)
        super.onCreate(savedInstanceState)
        // check for login state
        val target = if (session.isLoggedIn) {
            Routes.DASHBOARD_SCREEN
        } else {
            Routes.LOGIN_SCREEN
        }

        navigateToAndFinish(target)
    }
}