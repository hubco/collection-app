package com.cracklane.collection.app.data.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CollectionOtpVerificationResponse(
    val status: String,
    @SerializedName("t_id")
    val otpId: String
) : Parcelable