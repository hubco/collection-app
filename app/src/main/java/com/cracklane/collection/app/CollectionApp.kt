package com.cracklane.collection.app

import android.app.Application
import android.content.Context
import android.content.Intent
import android.location.Location
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.ui.view.activity.print.SunmiPrintHelper
import com.cracklane.collection.app.util.AudioPlayer
import com.sunmi.pay.hardware.aidlv2.print.PrinterOptV2
import com.sunmi.peripheral.printer.InnerPrinterCallback
import com.sunmi.peripheral.printer.InnerPrinterException
import com.sunmi.peripheral.printer.InnerPrinterManager
import com.sunmi.peripheral.printer.SunmiPrinterService
import dagger.hilt.android.HiltAndroidApp
import sunmi.paylib.SunmiPayKernel
import sunmi.paylib.SunmiPayKernel.ConnectCallback


@HiltAndroidApp
class CollectionApp : Application() {

    var location: Location? = null

    var printerOptV2: PrinterOptV2? = null
    var sunmiPrinterService: SunmiPrinterService? = null
    private var connectPaySDK = false

    override fun onCreate() {
        super.onCreate()
        appContext = this
        bindPrintService()
        init()
        // play intro audio if flag is true
        if (BuildConfig.PLAY_INTRO_AUDIO) {
            AudioPlayer.playIntro(this)
        }
    }

    private fun init() {
        SunmiPrintHelper.getInstance()?.initSunmiPrinterService(this)
    }

    /** bind printer service  */
    private fun bindPrintService() {
        try {
            InnerPrinterManager.getInstance().bindService(this, object : InnerPrinterCallback() {
                override fun onConnected(service: SunmiPrinterService) {
                    sunmiPrinterService = service
                }

                override fun onDisconnected() {
                    sunmiPrinterService = null
                }
            })
        } catch (e: InnerPrinterException) {
            e.printStackTrace()
        }
    }

    companion object {
        lateinit var appContext: Context

        fun notifySessionExpired() {
            LocalBroadcastManager.getInstance(appContext).sendBroadcastSync(Intent().apply {
                action = Constants.ACTION_SESSION_EXPIRED
            })
        }
    }

    fun isConnectPaySDK(): Boolean {
        return connectPaySDK
    }

    fun bindPaySDKService() {
        val payKernel = SunmiPayKernel.getInstance()
        payKernel.initPaySDK(this, object : ConnectCallback {
            override fun onConnectPaySDK() {
//                LogUtil.e(Constant.TAG, "onConnectPaySDK...")
                printerOptV2 = payKernel.mPrinterOptV2
                connectPaySDK = true
            }

            override fun onDisconnectPaySDK() {
//                LogUtil.e(Constant.TAG, "onDisconnectPaySDK...")
                connectPaySDK = false
                printerOptV2 = null
//                Utility.showToast(R.string.connect_fail)
            }
        })
    }

    fun hasLocationData(): Boolean = location != null

}