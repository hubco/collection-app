package com.cracklane.collection.app.common.extensions.dialog

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.architecture.singleClickEvent
import com.cracklane.collection.app.common.extensions.loadGif
import com.cracklane.collection.app.databinding.DialogOtpVerificationBinding
import com.cracklane.collection.app.databinding.ViewAlertSuccessfulBinding
import com.cracklane.collection.app.ui.view.widget.otptextview.OTPListener
import java.util.*


fun Context.alert(
    messageResource: Int,
    titleResource: Int? = null,
    init: (AlertBuilder<DialogInterface>.() -> Unit)? = null
): AlertBuilder<DialogInterface> {
    return AppThemeAlertBuilder(this).apply {
        if (titleResource != null) {
            this.titleResource = titleResource
        }
        this.messageResource = messageResource
        if (init != null) init()
    }
}

fun Context.alert(init: AlertBuilder<DialogInterface>.() -> Unit): AlertBuilder<DialogInterface> =
    AppThemeAlertBuilder(this).apply { init() }

fun Context.genericAlert(): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title = getString(R.string.alert_title_error)
        message = getString(R.string.alert_generic_error_message)
        positiveButton(android.R.string.ok) {}
    }.show()

fun Context.errorAlert(error: String?, callback: (() -> Unit)? = null): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title = getString(R.string.alert_title_error)
        message = error ?: getString(R.string.alert_generic_error_message)
        positiveButton(android.R.string.ok) { callback?.invoke() }
    }.show()

fun Context.confirmAlert(
    msg: String?,
    callback: ((confirm: Boolean) -> Unit)? = null
): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title = getString(R.string.alert_title_confirm)
        message = msg ?: getString(R.string.alert_generic_confirmation)
        positiveButton(android.R.string.ok) { callback?.invoke(true) }
        negativeButton(android.R.string.cancel) {
            callback?.invoke(false)
        }
    }.show()

fun Context.bluetoothConfirmationAlert(
    msg: String? = null,
    callback: (() -> Unit)? = null
): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title = getString(R.string.alert_title_bluetooth)
        message = msg ?: getString(R.string.alert_msg_bluetooth)
        positiveButton(android.R.string.ok) { callback?.invoke() }
        negativeButton(android.R.string.cancel) {

        }
    }.show()

fun Context.infoAlert(
    message: String? = getString(R.string.alert_message_action_completed),
    title: String? = getString(R.string.alert_title_info),
    callback: (() -> Unit)? = null
): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title?.let {
            this.title = it
        }
        message?.let {
            this.message = it
        }
        positiveButton(android.R.string.ok) { callback?.invoke() }
    }.show()

fun Context.successAlert(
    message: String?,
    title: String? = getString(R.string.alert_title_success),
    callback: (() -> Unit)? = null
): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title?.let {
            this.title = it
        }
        val binding =
            ViewAlertSuccessfulBinding.inflate(LayoutInflater.from(this@successAlert))
        binding.ivSuccess.loadGif(R.drawable.ic_successful)
        binding.tvMessage.text = message
        customView = binding.root

        positiveButton(android.R.string.ok) {
            callback?.invoke()
        }
    }.show()

fun Context.otpAlert(
    message: String?,
    otpPrefix: String?,
    callback: ((isResendOtp: Boolean, otpCode: String?) -> Unit)? = null
): AlertDialog {
    var alertDialog: AlertDialog? = null
    alertDialog = AppThemeAlertBuilder(this).apply {
        title = getString(R.string.title_verify_otp)
        val binding =
            DialogOtpVerificationBinding.inflate(LayoutInflater.from(this@otpAlert))
        binding.tvMessage.text = message
        binding.tvPrefix.text = otpPrefix
        binding.btnVerify.isEnabled = false

        val timer = object : CountDownTimer(40000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                if (binding.btnResendOtp.isEnabled) {
                    binding.btnResendOtp.isEnabled = false
                    binding.tvTimer.visibility = View.VISIBLE
                }
                binding.tvTimer.text = String.format(
                    getString(R.string.label_retry_in),
                    "${millisUntilFinished / 1000}"
                )
            }

            override fun onFinish() {
                binding.btnResendOtp.isEnabled = true
                binding.tvTimer.visibility = View.INVISIBLE
            }
        }.start()

        binding.otpView.otpListener = object : OTPListener {
            override fun onInteractionListener() {
                binding.btnVerify.isEnabled = false
            }

            override fun onOTPComplete(otp: String) {
                binding.btnVerify.isEnabled = true
            }
        }
        customView = binding.root

        binding.btnResendOtp.singleClickEvent {
            callback?.invoke(true, null)
        }

        binding.btnVerify.singleClickEvent {
            callback?.invoke(false, binding.otpView.otp)
        }

        binding.btnCancel.singleClickEvent {
            alertDialog?.dismiss()
        }

        alertDialog?.setOnDismissListener {
            timer.cancel()
        }

    }.show()
    return alertDialog

}

fun Context.printAlert(
    message: String? = getString(R.string.alert_message_action_completed),
    title: String? = getString(R.string.alert_title_info),
    printCallback: (() -> Unit)? = null,
    okCallback: (() -> Unit)? = null
): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title?.let {
            this.title = it
        }
        message?.let {
            this.message = it
        }
        positiveButton(getString(R.string.label_print)) {
            printCallback?.invoke()
        }
        negativeButton(android.R.string.ok) { okCallback?.invoke() }
        isCancelable = false
    }.show()

fun Context.datePickerDialog(callback: ((date: String) -> Unit)) {
    val calendar = Calendar.getInstance()
    DatePickerDialog(
        this,
        { _, year, monthOfYear, dayOfMonth ->
            callback("$dayOfMonth/${monthOfYear + 1}/$year")
        },
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH)
    ).also {
        it.datePicker.maxDate = calendar.timeInMillis
    }.show()
}