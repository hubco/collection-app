package com.cracklane.collection.app.ui.view.activity.enquiry

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.common.architecture.SingleLiveEvent
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.request.OtpVerificationRequest
import com.cracklane.collection.app.data.remote.response.NameValuePair
import com.cracklane.collection.app.domain.repository.IEnquiryRepository
import com.cracklane.collection.app.domain.repository.ISettingsRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EnquiryViewModel @Inject constructor(
    private val settingsRepository: ISettingsRepository,
    private val enquiryRepository: IEnquiryRepository
) : BaseViewModel() {

    val firstName = MutableLiveData<String>()
    val lastName = MutableLiveData<String>()
    val aadharNumber = MutableLiveData<String>()
    val panNumber = MutableLiveData<String>()
    val mobileNumber = MutableLiveData<String>()
    val dob = MutableLiveData<String>()
    val address = MutableLiveData<String>()
    val remarks = MutableLiveData<String>()

    val accountType = MutableLiveData<String>()

    val isSubmitButtonEnabled = MediatorLiveData<Boolean>().apply {
        addSource(firstName) {
            value = hasAllRequiredData
        }
        addSource(lastName) {
            value = hasAllRequiredData
        }
        addSource(aadharNumber) {
            value = hasAllRequiredData
        }
        addSource(panNumber) {
            value = hasAllRequiredData
        }
        addSource(mobileNumber) {
            value = hasAllRequiredData
        }
        addSource(dob) {
            value = hasAllRequiredData
        }
        addSource(address) {
            value = hasAllRequiredData
        }
    }

    private val hasAllRequiredData: Boolean
        get() = !accountType.value.isNullOrEmpty() && !firstName.value.isNullOrEmpty()
                && !lastName.value.isNullOrEmpty() && !aadharNumber.value.isNullOrEmpty()
                && aadharNumber.value!!.length == 12 && !panNumber.value.isNullOrEmpty()
                && panNumber.value!!.length == 10 && !mobileNumber.value.isNullOrEmpty()
                && mobileNumber.value!!.length == 10 && !dob.value.isNullOrEmpty()
                && !address.value.isNullOrEmpty()

    private var localAccountList = mutableListOf<NameValuePair>()
    private var _accountList = MutableLiveData<List<String>>()
    val accountList: LiveData<List<String>> = _accountList

    val otpGeneratedEvent = SingleLiveEvent<OtpVerificationRequest>()
    val otpGenerationFailureEvent = SingleLiveEvent<String>()

    val otpVerificationInProgress = SingleLiveEvent<Boolean>()
    val otpVerificationSuccessEvent = SingleLiveEvent<String>()
    val otpVerificationFailureEvent = SingleLiveEvent<String>()

    var photo: String? = null

    init {
        viewModelScope.launch {
            settingsRepository.getAccounts()?.let {
                addAccountList(it)
            }
        }
    }

    private fun addAccountList(it: List<NameValuePair>) {
        localAccountList.addAll(it)
        _accountList.value = localAccountList.map { it.name }
    }

    fun onSubmitButtonClicked() {
        generateOtp()
    }

    fun regenerateOtp() {
        generateOtp()
    }

    fun verifyOtp(otpId: String, otpCode: String) {
        viewModelScope.launch {
            otpVerificationInProgress.value = true
            when (val result = enquiryRepository.verifyOtp(otpId, otpCode)) {
                is Result.Success -> otpVerificationSuccessEvent.value = result.data.message ?: ""
                is Result.Error -> otpVerificationFailureEvent.value = result.error
            }
            otpVerificationInProgress.value = false
        }
    }

    private fun generateOtp() {
        viewModelScope.launch {
            loading(true)
            when (val result = enquiryRepository.postEntry(
                accountType.value!!,
                firstName.value!!,
                lastName.value!!,
                aadharNumber.value!!,
                panNumber.value!!,
                address.value!!,
                mobileNumber.value!!,
                dob.value!!,
                remarks.value ?: "",
                photo
            )) {
                is Result.Success -> otpGeneratedEvent.value = result.data!!
                is Result.Error -> otpGenerationFailureEvent.value = result.error
            }
            loading(false)
        }
    }
}