package com.cracklane.collection.app.ui.view.activity.transaction

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.data.remote.response.TransactionData
import com.cracklane.collection.app.databinding.ActivityTransactionListBinding
import com.cracklane.collection.app.ui.fragment.base.dialog.FILTER_DIALOG
import com.cracklane.collection.app.ui.fragment.base.dialog.FilterDialogFragment
import com.cracklane.collection.app.ui.navigation.Routes
import com.cracklane.collection.app.ui.navigation.navigateTo
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class TransactionListActivity : BaseActivity<ActivityTransactionListBinding>() {

    override val viewModel: TransactionViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_transaction_list

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    private val filterDialog by lazy {
        FilterDialogFragment { accountType, transactionType, startDate, endDate ->
            viewModel.getTransactions(accountType, transactionType, startDate, endDate)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getTransactions()

        binding.callback = object : TransactionItemCallback {
            override fun onItemClicked(data: TransactionData) {
                if (data.isPrint)
                    navigateTo(Routes.PRINT_COLLECTION_SCREEN,
                        mutableMapOf<String, Any>().apply {
                            put(Constants.EXTRA_FROM_TRANSACTION_SCREEN, true)
                            put(Constants.EXTRA_TRANSACTION_DATA, data)
                        })
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_calendar, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_calendar) {
            // show filter
            filterDialog.show(supportFragmentManager, FILTER_DIALOG)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun registerObserver() {}
}

@BindingAdapter("transactionList", "callback", requireAll = false)
fun RecyclerView.setTransactions(
    result: List<TransactionData>?,
    callback: TransactionItemCallback?
) {
    result?.let {
        addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayoutManager.VERTICAL
            )
        )
        layoutManager = LinearLayoutManager(context)
        adapter = TransactionListAdapter(it) { data ->
            callback?.onItemClicked(data)
        }
    }
}

interface TransactionItemCallback {
    fun onItemClicked(data: TransactionData)
}