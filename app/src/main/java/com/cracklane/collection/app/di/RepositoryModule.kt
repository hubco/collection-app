package com.cracklane.collection.app.di

import com.cracklane.collection.app.data.local.db.dao.AppDao
import com.cracklane.collection.app.data.remote.service.*
import com.cracklane.collection.app.data.repository.*
import com.cracklane.collection.app.domain.repository.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    fun provideAuthRepository(authService: AuthService): IAuthRepository =
        AuthRepository(authService)

    @Provides
    fun provideSettingsRepository(
        settingsService: SettingsService,
        dao: AppDao
    ): ISettingsRepository =
        SettingsRepository(settingsService, dao)

    @Provides
    fun provideAccountRepository(service: AccountService): IAccountRepository =
        AccountRepository(service)

    @Provides
    fun provideUserRepository(service: UserService): IUserRepository = UserRepository(service)

    @Provides
    fun provideEnquiryRepository(service: EnquiryService): IEnquiryRepository = EnquiryRepository(service)
}