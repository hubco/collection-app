package com.cracklane.collection.app.common

object Constants {

    const val ACTION_SESSION_EXPIRED = "action_session_expired"
    const val EXTRA_OTP_VERIFICATION_DATA = "extra_otp_verification_data"

    const val EXTRA_COLLECTION_TYPE = "extra_collection_type"
    const val EXTRA_ACCOUNT_TYPE = "extra_account_type"
    const val EXTRA_ACCOUNT_INFO = "extra_account_info"
    const val EXTRA_COLLECTION_RESPONSE = "extra_collection_response"
    const val EXTRA_FROM_TRANSACTION_SCREEN = "extra_from_transaction_screen"
    const val EXTRA_TRANSACTION_DATA = "extra_transaction_data"
    const val EXTRA_COLLECTION_DATA = "EXTRA_COLLECTION_DATA"

    const val OTP_DEPOSIT_VALUE = "credit"
    const val OTP_WITHDRAWAL_VALUE = "debit"

    const val TITLE_CASH_DEPOSIT = "Cash Deposit"
    const val TITLE_OTP_DEPOSIT = "OTP Deposit"
    const val TITLE_OTP_WITHDRAW = "OTP Withdraw"
}