package com.cracklane.collection.app.ui.view.activity.dashboard

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.SwitchCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.lifecycleScope
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.Biometric
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.common.extensions.dialog.alert
import com.cracklane.collection.app.common.extensions.dialog.errorAlert
import com.cracklane.collection.app.common.extensions.load
import com.cracklane.collection.app.common.extensions.toast
import com.cracklane.collection.app.databinding.ActivityDashboardBinding
import com.cracklane.collection.app.ui.navigation.Routes
import com.cracklane.collection.app.ui.navigation.navigateTo
import com.cracklane.collection.app.ui.navigation.navigateToLogin
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_dashboard.view.*
import kotlinx.android.synthetic.main.common_tool_bar.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class DashboardActivity : BaseActivity<ActivityDashboardBinding>() {

    override val viewModel: DashboardViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_dashboard

    override val toolBarId: Int = R.id.commonToolBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val header = drawer.navView.getHeaderView(0)
        header.tvName.text = session.userName
        header.ivAvatar.load(session.userImage, R.drawable.ic_person)

        // setup navigation drawer
        val toggle = ActionBarDrawerToggle(
            this, drawer, commonToolBar,
            R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        val biometricSwitch =
            (navView.menu.findItem(R.id.nav_biometric).actionView as SwitchCompat?)
        // show biometric login only if device supported
        Biometric.isAvailable.run {
            navView.menu.setGroupVisible(R.id.biometric, this)
            biometricSwitch?.isChecked = session.isBiometricEnabled
        }

        biometricSwitch?.setOnClickListener {
            authenticateBiometricLogin {
                biometricSwitch.isChecked = it
            }
        }

        if(!Biometric.isAvailable)
        navView.menu.removeItem(R.id.biometric)

        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_cash_deposit -> closeDrawer {
                    navigateTo(Routes.COLLECTION_SCREEN)
                }
                R.id.nav_otp_deposit -> closeDrawer {
                    navigateTo(Routes.OTP_DEPOSIT_SCREEN)
                }
                R.id.nav_otp_withdrawal -> closeDrawer {
                    navigateTo(Routes.OTP_WITHDRAW_SCREEN)
                }
                R.id.nav_collection_list -> closeDrawer {
                    navigateTo(Routes.COLLECTION_LIST_SCREEN)
                }
                R.id.nav_transaction_list -> closeDrawer {
                    navigateTo(Routes.TRANSACTION_LIST_SCREEN)
                }
                R.id.nav_enquiry -> closeDrawer {
                    navigateTo(Routes.ENQUIRY_SCREEN)
                }
                R.id.nav_settings -> closeDrawer {
                    navigateTo(Routes.SETTINGS_SCREEN)
                }
                R.id.nav_biometric -> biometricSwitch?.performClick()

                R.id.nav_change_password -> closeDrawer {
                    navigateTo(Routes.CHANGE_PASSWORD_SCREEN)
                }
                R.id.nav_logout -> closeDrawer {
                    confirmLogout()
                }
            }
            true
        }

        binding.swipeRefresh.setOnRefreshListener {
            viewModel.loadPageLimit()
            binding.swipeRefresh.isRefreshing = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_profile, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_profile) {
            navigateTo(Routes.USER_PROFILE_SCREEN)
        } else if (item.itemId == R.id.action_collections) {
            navigateTo(Routes.COLLECTION_LIST_SCREEN)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun registerObserver() {
        super.registerObserver()
        viewModel.collectionTypeEvent.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                navigateTo(Routes.COLLECTION_SCREEN,
                    mutableMapOf<String, Int>().apply {
                        put(Constants.EXTRA_COLLECTION_TYPE, it.ordinal)
                    })
            }
        }

        viewModel.pageLimitFailureEvent.observe(this) {
            errorAlert(it)
        }

        viewModel.isCashDepositEnabled.observe(this) { visible ->
            if (!visible) {
                navView.menu.removeItem(R.id.nav_cash_deposit)
            }
        }
        viewModel.isCashDepositOtpEnabled.observe(this) { visible ->
            if (!visible) {
                navView.menu.removeItem(R.id.nav_otp_deposit)
            }
        }
        viewModel.isCashWithdrawalOtpEnabled.observe(this) { visible ->
            if (!visible) {
                navView.menu.removeItem(R.id.nav_otp_withdrawal)
            }
        }
        viewModel.isEnquiryEnabled.observe(this) { visible ->
            if (!visible) {
                navView.menu.removeItem(R.id.nav_enquiry)
            }
        }
    }

    private fun closeDrawer(callback: (() -> Unit)? = null) {
        drawer.closeDrawer(GravityCompat.START)
        lifecycleScope.launch {
            delay(300)
            callback?.invoke()
        }
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            closeDrawer()
        } else {
            super.onBackPressed()
        }
    }

    private fun authenticateBiometricLogin(callback: ((success: Boolean) -> Unit)?) {
        Biometric.authenticate(this) { authenticated, error ->
            if (authenticated) {
                val newValue = session.isBiometricEnabled
                session.setBiometricLogin = newValue
                callback?.invoke(newValue)
            } else {
                toast(error ?: getString(R.string.biometric_authentication_error))
                callback?.invoke(session.isBiometricEnabled)
            }
        }
        session.setBiometricLogin = !session.isBiometricEnabled
    }

    private fun confirmLogout() {
        alert {
            title = getString(R.string.nav_logout)
            message = getString(R.string.title_logout_confirm)
            positiveButton(android.R.string.yes) {
                session.logout()
                navigateToLogin()
            }
            negativeButton(android.R.string.no) {}
        }.show()
    }
}