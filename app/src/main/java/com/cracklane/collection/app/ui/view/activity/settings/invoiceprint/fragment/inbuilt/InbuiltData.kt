package com.cracklane.collection.app.ui.view.activity.settings.invoiceprint.fragment.inbuilt

data class InbuiltData(
    var printerName: String?,
    var printerCode: String?
)

val INBUILT_PRINTER_LIST : ArrayList<InbuiltData> = arrayListOf<InbuiltData>().apply {
    add(InbuiltData(printerName = "Sunmi P2", printerCode = "sunmi"))
}