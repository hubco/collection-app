package com.cracklane.collection.app.ui.view.activity.settings

import com.cracklane.collection.app.common.architecture.SingleLiveEvent
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(

) : BaseViewModel() {

    val invoicePrintClickEvent = SingleLiveEvent<Void>()

    fun onInvoicePrintClicked() {
        invoicePrintClickEvent.call()
    }
}