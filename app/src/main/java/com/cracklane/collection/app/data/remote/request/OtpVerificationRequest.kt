package com.cracklane.collection.app.data.remote.request

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OtpVerificationRequest(
    var userName: String,
    val otpId: String,
    val message: String?
) : Parcelable