package com.cracklane.collection.app.data.local.db

import androidx.room.TypeConverter
import com.cracklane.collection.app.data.remote.response.ActiveModules
import com.cracklane.collection.app.data.remote.response.BankAccounts
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converter {

    @TypeConverter
    fun fromStringToListOfList(value: String): List<List<String>> {
        val type = object : TypeToken<List<List<String>>>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun fromListOfListToString(list: List<List<String>>): String {
        val type = object : TypeToken<List<List<String>>>() {}.type
        return Gson().toJson(list, type)
    }

    @TypeConverter
    fun fromStringToList(value: String): List<String> {
        val type = object : TypeToken<List<String>>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun fromListToString(list: List<String>): String {
        val type = object : TypeToken<List<String>>() {}.type
        return Gson().toJson(list, type)
    }

    @TypeConverter
    fun fromActiveModuleToString(module: ActiveModules): String {
        val type = object : TypeToken<ActiveModules>() {}.type
        return Gson().toJson(module, type)
    }

    @TypeConverter
    fun fromStringToActiveModule(value: String): ActiveModules {
        val type = object : TypeToken<ActiveModules>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun fromBankAccountsToString(accounts: List<BankAccounts>): String {
        val type = object : TypeToken<List<BankAccounts>>() {}.type
        return Gson().toJson(accounts, type)
    }

    @TypeConverter
    fun fromStringToBankAccounts(value: String): List<BankAccounts> {
        val type = object : TypeToken<List<BankAccounts>>() {}.type
        return Gson().fromJson(value, type)
    }
}