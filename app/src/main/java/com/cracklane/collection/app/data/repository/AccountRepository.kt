package com.cracklane.collection.app.data.repository

import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.*
import com.cracklane.collection.app.data.remote.service.AccountService
import com.cracklane.collection.app.domain.repository.IAccountRepository

class AccountRepository(private val service: AccountService) : BaseRepository(),
    IAccountRepository {

    override suspend fun searchAccount(
        accountType: String,
        query: String
    ): Result<SearchAccountResponse> = makeNetworkCall { service.searchAccount(accountType, query) }

    override suspend fun getAccountInfo(
        accountType: String,
        slug: String
    ): Result<AccountInfoResponse> = makeNetworkCall { service.getAccountInfo(accountType, slug) }

    override suspend fun submitCollection(
        accountType: String,
        slug: String,
        amount: String,
        pMode: String,
        utr_no: String,
        t_date: String,
        bank_account_id: String,
        t_mode: String,
        bank_name: String,
        remarks: String
    ): Result<CollectionSubmitResponse> =
        makeNetworkCall { service.submitCollection(accountType, slug, amount, pMode, utr_no, t_date, bank_account_id,t_mode,bank_name, remarks) }

    override suspend fun submitCollectionOtp(
        accountType: String,
        slug: String,
        amount: String,
        tType: String
    ): Result<CollectionSubmitOtpResponse> =
        makeNetworkCall { service.submitCollectionOtp(accountType, slug, amount, tType) }

    override suspend fun resendOtp(otpId: String): Result<CollectionSubmitOtpResponse> {
        return makeNetworkCall { service.resendOtp(otpId) }
    }

    override suspend fun verifyOtp(
        otpId: String,
        otpCode: String?
    ): Result<CollectionSubmitResponse> {
        return makeNetworkCall { service.verifyOtp(otpId, otpCode) }
    }

    override suspend fun homePageLimit(): Result<HomePageLimitResponse> {
        return makeNetworkCall { service.getHomePageLimits() }
    }

    override suspend fun getTransactionList(
        accountType: String,
        transactionType: String,
        fromDate: String,
        toDate: String
    ): Result<TransactionResponse> {
        return makeNetworkCall {
            service.getTransactionList(
                accountType,
                transactionType,
                fromDate,
                toDate
            )
        }
    }

    override suspend fun getCollectionList(accountType: String, date: String): Result<CollectionResponse> {
        return makeNetworkCall { service.getCollectionList(accountType, date) }
    }
}