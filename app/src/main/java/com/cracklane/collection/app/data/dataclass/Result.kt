package com.cracklane.collection.app.data.dataclass

sealed class Result<out T> {
    data class Success<T>(val data: T) : Result<T>()
    data class Error(val error: String) : Result<Nothing>()
    data class Unauthorized(val error: String = "Session expired!") : Result<Nothing>()
}

