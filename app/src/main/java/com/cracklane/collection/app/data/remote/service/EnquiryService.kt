package com.cracklane.collection.app.data.remote.service

import com.cracklane.collection.app.data.remote.request.OtpVerificationRequest
import com.cracklane.collection.app.data.remote.response.GenericResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface EnquiryService {

    @POST("/api/agent/v1/inquiry/initiate-otp")
    @FormUrlEncoded
    suspend fun postEnquiry(
        @Field("inquiry_type") inquiryType: String,
        @Field("first_name") firstName: String,
        @Field("last_name") lastName: String,
        @Field("aadhaar_no") aadharNo: String,
        @Field("pan_no") panNo: String,
        @Field("address") address: String,
        @Field("mob_no") mobNo: String,
        @Field("dob") dob: String,
        @Field("remarks") remarks: String,
        @Field("photo") photo: String?
    ): Response<OtpVerificationRequest>

    @POST("/api/agent/v1/inquiry/verify-otp")
    @FormUrlEncoded
    suspend fun verifyOtp(
        @Field("otp_id") otpId: String,
        @Field("otp") otpCode: String
    ): Response<GenericResponse>
}