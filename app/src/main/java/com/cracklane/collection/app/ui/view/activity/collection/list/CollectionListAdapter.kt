package com.cracklane.collection.app.ui.view.activity.collection.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cracklane.collection.app.data.remote.response.CollectionData
import com.cracklane.collection.app.databinding.LayoutCollectionBinding

class CollectionListAdapter(
    private val list: List<CollectionData>,
    private val callback: ((data: CollectionData) -> Unit)? = null
) :
    RecyclerView.Adapter<CollectionListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CollectionListAdapter.ViewHolder {
        val binding = LayoutCollectionBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: CollectionListAdapter.ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class ViewHolder(val binding: LayoutCollectionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: CollectionData) {
            binding.memberInfo = "${data.acNo} - ${data.memberName} - ${data.memberNo} - ${data.acType}"
            binding.date = data.openDate
            binding.amount = "Amount to collect: ${data.amountToCollect}"
            binding.root.setOnClickListener {
                callback?.invoke(data)
            }
        }
    }
}