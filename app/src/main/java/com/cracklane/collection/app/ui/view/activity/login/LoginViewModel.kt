package com.cracklane.collection.app.ui.view.activity.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.BuildConfig
import com.cracklane.collection.app.common.Biometric
import com.cracklane.collection.app.common.architecture.SingleLiveEvent
import com.cracklane.collection.app.common.session.ISession
import com.cracklane.collection.app.data.dataclass.Event
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.LoginResponse
import com.cracklane.collection.app.domain.repository.IAuthRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: IAuthRepository,
    private val session: ISession
) :
    BaseViewModel() {

    val userNameData = MutableLiveData<String>()
    val passwordData = MutableLiveData<String>()

    val isBioMetricEnabled = MutableLiveData(session.isBiometricEnabled && Biometric.isAvailable)

    val isPoweredByEnabled = MutableLiveData(BuildConfig.SHOW_POWERED_BY)

    val loginButtonEnabled = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(userNameData) {
            value = hasLoginData()
        }
        addSource(passwordData) {
            value = hasLoginData()
        }
    }

    private fun hasLoginData() =
        !userNameData.value.isNullOrEmpty() && !passwordData.value.isNullOrEmpty()

    val forgotClickEvent = SingleLiveEvent<Void>()
    val quickLoginClickEvent = SingleLiveEvent<Void>()

    private val _loginSuccessEvent = SingleLiveEvent<Void>()
    val loginSuccessEvent: LiveData<Void> = _loginSuccessEvent

    private val _loginFailureEvent = MutableLiveData<Event<String>>()
    val loginFailureEvent: LiveData<Event<String>> = _loginFailureEvent

    fun onLoginButtonClicked() {
        viewModelScope.launch {
            loading(true)
            handleLoginResult(repository.login(userNameData.value!!, passwordData.value!!))
            loading(false)
        }
    }

    private fun handleLoginResult(result: Result<LoginResponse>) {
        when (result) {
            is Result.Success -> {
                // save user data
                session.saveUser(result.data)
                _loginSuccessEvent.call()
            }
            is Result.Error -> _loginFailureEvent.value = Event(result.error)
        }
    }

    fun onForgotClick() {
        forgotClickEvent.call()
    }

    fun onQuickLoginButtonClicked() {
        quickLoginClickEvent.call()
    }

    fun authenticateQuickLogin() {
        viewModelScope.launch {
            loading(true)
            handleLoginResult(repository.quickLogin())
            loading(false)
        }
    }

    override fun onCleared() {
        super.onCleared()
        loginButtonEnabled.removeSource(userNameData)
        loginButtonEnabled.removeSource(passwordData)
    }
}