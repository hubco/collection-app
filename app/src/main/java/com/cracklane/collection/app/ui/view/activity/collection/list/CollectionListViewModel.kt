package com.cracklane.collection.app.ui.view.activity.collection.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.CollectionData
import com.cracklane.collection.app.domain.repository.IAccountRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class CollectionListViewModel @Inject constructor(private val repository: IAccountRepository) :
    BaseViewModel() {

    private val _collectionList = mutableListOf<CollectionData>()
    private val _filteredCollectionList = MutableLiveData<List<CollectionData>>()
    val collectionList: LiveData<List<CollectionData>> = _filteredCollectionList

    private val _errorMessage = MutableLiveData<String?>()
    val errorMessage: LiveData<String?> = _errorMessage

    fun refresh() {
        getCollectionsForAccount()
    }

    fun queryData(query: CharSequence) {
        if(query.isNotBlank()) {
            _filteredCollectionList.value = _collectionList.filter { it.memberName.lowercase().contains(query.toString().lowercase()) || it.memberNo.contains(query) || it.acNo.contains(query) }
        } else {
            _filteredCollectionList.value = _collectionList
        }
    }

    fun getCollectionsForAccount(accountType: String = "") {
        viewModelScope.launch {
            val calendar = Calendar.getInstance()
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            val month = calendar.get(Calendar.MONTH)
            val year = calendar.get(Calendar.YEAR)
            val date =  "$day/${month + 1}/$year"

            _errorMessage.value = null
            _collectionList.clear()
            _filteredCollectionList.value = emptyList()
            loading(true)
            when (val result = repository.getCollectionList(accountType, date)) {
                is Result.Success -> {
                    _collectionList.addAll(result.data.response)
                    _filteredCollectionList.value = _collectionList
                    if (result.data.response.isEmpty()) {
                        _errorMessage.value = "No result found!"
                    }
                }
                is Result.Error -> _errorMessage.value = result.error
            }
            loading(false)
        }
    }
}