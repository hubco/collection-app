package com.cracklane.collection.app.data.dataclass

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}