package com.cracklane.collection.app.common.session

import android.content.Context
import android.content.SharedPreferences
import com.cracklane.collection.app.common.preference.Preferences
import com.cracklane.collection.app.common.preference.Preferences.get
import com.cracklane.collection.app.common.preference.Preferences.set
import com.cracklane.collection.app.data.remote.response.LoginResponse
import javax.inject.Inject

private const val IS_FIRST_LAUNCH = "pref_is_first_launch"
private const val LOGGED_IN = "pref_logged_in"
private const val BIOMETRIC_LOGIN_ENABLED = "pref_biometric_login_status"
private const val USER_ID = "pref_user_id"
private const val USER_NAME = "pref_user_name"
private const val USER_AVATAR = "pref_user_avatar"
private const val USER_TOKEN = "pref_user_phone"
private const val PRINTER_NAME = "pref_printer_name"
private const val PRINTER_CODE = "pref_printer_code"
private const val IS_INBUILT_PRINTER_SELECTED = "pref_inbuilt_printer_selection"

class SessionImpl @Inject constructor(context: Context) : ISession {

    private val prefs: SharedPreferences = Preferences.defaultPrefs(context)

    /**
     * Save User details on login
     * @param user - User model
     */
    override fun saveUser(user: LoginResponse) {
        prefs[USER_ID] = user.userId
        prefs[USER_NAME] = user.name
        prefs[USER_AVATAR] = user.imageUrl
        prefs[USER_TOKEN] = user.token
        prefs[LOGGED_IN] = true
        prefs[IS_FIRST_LAUNCH] = false
    }

    /**
     * Check if user is logged in
     */
    override val isLoggedIn get() = prefs[LOGGED_IN, false]

    /**
     * Check if is first launch
     */
    override val isFirstLaunch get() = prefs[IS_FIRST_LAUNCH, true]

    /**
     * Get the logged in User ID
     */
    override val userId: String get() = prefs[USER_ID, ""]

    /**
     * Get the logged in User token
     */
    override val userToken: String get() = prefs[USER_TOKEN, ""]

    /**
     * Get the logged in User username
     */
    override val userName: String get() = prefs[USER_NAME, ""]

    /**
     * Get the logged in User image
     */
    override val userImage: String get() = prefs[USER_AVATAR, ""]

    /**
     * Get the logged in User
     */
    override val user: LoginResponse
        get() = LoginResponse(
            userId = prefs[USER_ID, ""],
            name = prefs[USER_NAME, ""],
            imageUrl = prefs[USER_AVATAR, ""],
            token = prefs[USER_TOKEN, ""],
            type = ""
        )

    override var isBiometricEnabled: Boolean
        get() = prefs[BIOMETRIC_LOGIN_ENABLED, false]
        set(value) {
            prefs[BIOMETRIC_LOGIN_ENABLED] = value
        }
    override var setBiometricLogin: Boolean
        get() = TODO("Not yet implemented")
        set(value) {
            prefs[BIOMETRIC_LOGIN_ENABLED] = value
        }

    override var printerCode: String
        get() = prefs[PRINTER_CODE, ""]
        set(value) {
            prefs[PRINTER_CODE] = value
        }

    override var printerName: String
        get() = prefs[PRINTER_NAME, ""]
        set(value) {
            prefs[PRINTER_NAME] = value
        }

    override var isInbuiltPriterSelected: Boolean
        get() = prefs[IS_INBUILT_PRINTER_SELECTED, false]
        set(value) {
            prefs[IS_INBUILT_PRINTER_SELECTED] = value
        }

    /**
     * Update session as logout but keep user details
     * for quick login
     */
    override fun logout() {
        prefs[LOGGED_IN] = false
       // setBiometricLogin = false
    }

    /**
     * Function to clear prefs
     */
    override fun clearSession() {
        prefs.edit().clear().apply()
    }
}