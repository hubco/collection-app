package com.cracklane.collection.app.data.remote.response

data class AgentInfoResponse(
    val aadharNo: String?,
    val agentCode: String?,
    val agentRank: String?,
    val dob: String?,
    val enrollmentDate: String?,
    val fatherName: String?,
    val imageUrl: String?,
    val mobileNo: String?,
    val name: String?,
    val nomineeName: String?,
    val nomineeRelation: String?,
    val panNo: String?,
    val permanentAddress: String?,
    val spouseName: String?,
    val status: String?,
    val userId: String?
)