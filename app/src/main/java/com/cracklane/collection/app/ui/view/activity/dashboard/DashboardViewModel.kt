package com.cracklane.collection.app.ui.view.activity.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.common.CollectionType
import com.cracklane.collection.app.common.architecture.SingleLiveEvent
import com.cracklane.collection.app.data.dataclass.Event
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.domain.repository.IAccountRepository
import com.cracklane.collection.app.domain.repository.ISettingsRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(
    private val settingsRepository: ISettingsRepository,
    private val accountRepository: IAccountRepository
) : BaseViewModel() {

    private val _collectionLimit = MutableLiveData("0")
    val collectionLimit: LiveData<String> = _collectionLimit

    private val _availableLimit = MutableLiveData("0")
    val availableLimit: LiveData<String> = _availableLimit

    private val _onlinePayment = MutableLiveData("0")
    val onlinePayment: LiveData<String> = _onlinePayment

    private val _cashInHand = MutableLiveData("0")
    val cashInHand: LiveData<String> = _cashInHand

    private val _collections = MutableLiveData("0")
    val collections: LiveData<String> = _collections

    private val _branchCashDeposit = MutableLiveData("0")
    val branchCashDeposit: LiveData<String> = _branchCashDeposit

    private val _collectionTypeEvent = MutableLiveData<Event<CollectionType>>()
    val collectionTypeEvent: LiveData<Event<CollectionType>> = _collectionTypeEvent

    val pageLimitFailureEvent = SingleLiveEvent<String>()

    val isCashDepositEnabled = MutableLiveData(true)
    val isCashDepositOtpEnabled = MutableLiveData(true)
    val isCashWithdrawalOtpEnabled = MutableLiveData(true)
    val isEnquiryEnabled = MutableLiveData(true)

    init {
        loadSettings()
    }

    private fun loadSettings() {
        viewModelScope.launch {
            loading(true)
            val result = settingsRepository.loadSettings()
            if (result is Result.Success) {
                isCashDepositEnabled.value = result.data.activeModules.isCashDepositEnabled
                isCashDepositOtpEnabled.value = result.data.activeModules.isCashDepositOtpEnabled
                isCashWithdrawalOtpEnabled.value =
                    result.data.activeModules.isCashWithdrawalOtpEnabled
                isEnquiryEnabled.value = result.data.activeModules.isEnquiryFormEnabled
            }
            loadPageLimit()
            loading(false)
        }
    }

    fun onDepositClicked() {
        _collectionTypeEvent.value = Event(CollectionType.SINGLE)
    }

    fun onOtpDepositClicked() {
        _collectionTypeEvent.value = Event(CollectionType.OTP_DEPOSIT)
    }

    fun onOtpWithdrawalClicked() {
        _collectionTypeEvent.value = Event(CollectionType.OTP_WITHDRAWAL)
    }

    fun loadPageLimit() {
        viewModelScope.launch {
            loading(true)
            when (val result =
                accountRepository.homePageLimit()) {
                is Result.Success -> {
                    result.data.let {
                        _collectionLimit.value = it.collectionLimit ?: "0"
                        _availableLimit.value = it.availableLimit ?: "0"
                        _onlinePayment.value = it.otherPayment ?: "0"
                        _cashInHand.value = it.cashInHand ?: "0"
                        _collections.value = it.pendingCollection ?: "0"
                        _branchCashDeposit.value = it.branchCashDeposit ?: "0"
                    }
                }
                is Result.Error -> pageLimitFailureEvent.value = result.error
            }
            loading(false)
        }
    }

}