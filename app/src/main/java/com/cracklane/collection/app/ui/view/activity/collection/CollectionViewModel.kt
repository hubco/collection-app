package com.cracklane.collection.app.ui.view.activity.collection

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.common.CollectionType
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.common.architecture.NotifyChangeLiveData
import com.cracklane.collection.app.common.architecture.SingleLiveEvent
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.*
import com.cracklane.collection.app.domain.repository.IAccountRepository
import com.cracklane.collection.app.domain.repository.ISettingsRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CollectionViewModel @Inject constructor(
    private val settingsRepository: ISettingsRepository,
    private val accountRepository: IAccountRepository
) : BaseViewModel() {

    private val _title = MutableLiveData<String>()
    val title: LiveData<String> = _title

    private var _accountList = MutableLiveData<List<String>>()
    val accountList: LiveData<List<String>> = _accountList

    private val _isAccountDetailsLoading = MutableLiveData<Boolean>()
    val isAccountDetailsLoading: LiveData<Boolean> = _isAccountDetailsLoading

    private val _paymentModes = MutableLiveData<List<String>>()
    val paymentModes: LiveData<List<String>> = _paymentModes

    private var _paymentCodes = emptyList<String>()
    private val selectedPaymentMode = MutableLiveData<String>()

    val accountType = NotifyChangeLiveData<String> {
        if (!hasCollectionData.value!!)
            notifyAccountTypeChanged()
    }
    val accountNumber = MutableLiveData<String>()
    val memberName = MutableLiveData<String>()
    val memberNumber = MutableLiveData<String>()
    val memberScheme = MutableLiveData<String>()
    val openDate = MutableLiveData<String>()
    val amountBalance = MutableLiveData<String>()
    val amountToPay = MutableLiveData<String>()
    val amount = MutableLiveData<String>()
    val remarks = MutableLiveData<String>()
    val utrNo = MutableLiveData<String>()
    val transactionDate = MutableLiveData<String>()
    val toolbarTitle = MutableLiveData<String>()
    private val _selectedTransactionMode = MutableLiveData<String>()
    var slug: String = ""
    val hasAccountDetails = MutableLiveData<Boolean>()

    private var _collectionType = CollectionType.SINGLE
    val isAccountTypeRequired = MutableLiveData<Boolean>()

    var accountInfo: AccountInfoResponse? = null
        set(value) {
            value?.let {
                field = it
                memberName.value = it.memberName
                memberNumber.value = it.memberNo
                memberScheme.value = it.scheme
                accountNumber.value = it.acNo
                openDate.value = it.openDate
                amountBalance.value = it.acBalance ?: ""
                amountToPay.value = it.amountToCollect
                slug = it.acSlug
                hasAccountDetails.value = true
            }
        }

    var hasCollectionData = MutableLiveData(false)

    var selectedAccountCode: String? = null
        get() = if (field == null) {
            localAccountList.find { it.name == accountType.value }?.value
        } else {
            field
        }
        set(value) {
            field = value
            accountType.value = localAccountList.find { it.value == field }?.name
        }

    private var _isSubmitButtonEnabled = MediatorLiveData<Boolean>().apply {
        addSource(accountType) {
            value = hasAllRequiredData
        }
        addSource(accountNumber) {
            value = hasAllRequiredData
        }
        addSource(amount) {
            value = hasAllRequiredData
        }
        addSource(selectedPaymentMode) {
            isCashPaymentSelected.value = selectedPaymentMode.value?.lowercase() == "cash"
            value = hasAllRequiredData
        }
        addSource(utrNo){
            value = hasAllRequiredData
        }
        addSource(transactionDate){
            value = hasAllRequiredData
        }
        addSource(_selectedTransactionMode){
            value = hasAllRequiredData
        }
    }
    val isSubmitButtonEnabled: LiveData<Boolean> = _isSubmitButtonEnabled

    val isNonCashPaymentSelected = MutableLiveData(false)
    val isChequeSelected = MutableLiveData(false)
    val isBankListNotNull = MutableLiveData(true)
    val isBankAccountListNotNull = MutableLiveData(false)
    val isOnlineTransferSelected = MutableLiveData(false)
    private val isCashPaymentSelected = MutableLiveData(false)
    val selectedBankAccountType = MutableLiveData<String>()
    private val _selectedBankAccountId by lazy {
        _bankAccounts.find { it.bankName == selectedBankAccountType.value }?.bankId
    }
    val selectedBankName = MutableLiveData<String>()
    private val _selectedBankName: MutableLiveData<String> = selectedBankName




    private val hasAllRequiredData: Boolean
        get() = if (_collectionType == CollectionType.SINGLE)
            if (isCashPaymentSelected.value == true)
                !accountNumber.value.isNullOrEmpty() && !amount.value.isNullOrEmpty()
            else
                !accountNumber.value.isNullOrEmpty() && !amount.value.isNullOrEmpty()  && !utrNo.value.isNullOrEmpty() && !transactionDate.value.isNullOrEmpty()

        else !accountType.value.isNullOrEmpty() && !accountNumber.value.isNullOrEmpty()
                && !amount.value.isNullOrEmpty()

    private var localAccountList = mutableListOf<NameValuePair>()

    val selectAccountClickEvent = SingleLiveEvent<Void>()
    val fetchAccountInfoFailureEvent = SingleLiveEvent<String>()
    val collectionSubmitSuccessEvent = SingleLiveEvent<CollectionSubmitResponse>()
    val collectionSubmitFailureEvent = SingleLiveEvent<String>()
    private val _bankAccounts = mutableListOf<BankAccounts>()
    private var _bankAccountList = MutableLiveData<List<String>>()
    val bankAccountList: LiveData<List<String>> = _bankAccountList

    private var _bankList = MutableLiveData<List<String>>()
    var bankList : LiveData<List<String>> = _bankList

    private var _transactionModePairList = mutableListOf<NameValuePair>()

    private var _transactionModes = MutableLiveData<List<String>>()
    var transactionModes : LiveData<List<String>> = _transactionModes

    private val _otpRequestResultData = MutableLiveData<Result<CollectionSubmitOtpResponse>>()
    val otpRequestResultData: LiveData<Result<CollectionSubmitOtpResponse>>
        get() = _otpRequestResultData

    init {
        // load payment modes
        viewModelScope.launch {
            settingsRepository.getPaymentModes()?.let { modes ->
                _paymentCodes = modes
                _paymentModes.value = _paymentCodes.map { it.toSimplifiedPaymentMode() }
            }

            settingsRepository.getTransferModes()?.let { tModeList ->
                _transactionModePairList.clear()
                _transactionModePairList.addAll(tModeList)
                _transactionModes.value = _transactionModePairList.map { it.name }
            }
            settingsRepository.getBankAccounts()?.let { accounts ->
                isBankAccountListNotNull.value = accounts.isNotEmpty()
            }
        }
    }

    fun setTitle(title: String) {
        _title.value = title
    }

    fun onPaymentModeChanged(index: Int) {
        selectedPaymentMode.value = _paymentCodes[index]
        isCashPaymentSelected.value = selectedPaymentMode.value?.lowercase() == "cash"
        isNonCashPaymentSelected.value = selectedPaymentMode.value?.lowercase() != "cash"
        isChequeSelected.value = selectedPaymentMode.value?.lowercase() == "cheque"
        isOnlineTransferSelected.value = selectedPaymentMode.value?.lowercase() == "online_transfer"//== "online_transfer"
        if (isNonCashPaymentSelected.value == true && _bankAccounts.isEmpty()) {
            viewModelScope.launch {
                settingsRepository.getBankAccounts()?.let { accounts ->
                    if (accounts.isNotEmpty()) {
                        _bankAccounts.clear()
                        _bankAccounts.addAll(accounts)
                        _bankAccountList.value = _bankAccounts.map { it.bankName }
                    } else {
                        isNonCashPaymentSelected.value = false
                        isOnlineTransferSelected.value = false
                    }
                    _bankList.value = settingsRepository.getBankList()
                    isBankListNotNull.value = _bankList.value?.isNotEmpty() ?: false
                }
            }
        }

    }

    fun onTransactionModeChanged(index: Int){
        _selectedTransactionMode.value = _transactionModePairList[index].value
    }

    fun setCollectionType(type: CollectionType, callback: (() -> Unit)? = null) {
        _collectionType = type
        viewModelScope.launch {
            when (type) {
                CollectionType.SINGLE -> {
                    toolbarTitle.value = Constants.TITLE_CASH_DEPOSIT
                    settingsRepository.getAccounts()?.let {
                        addAccountList(it)
                    }
                }
                CollectionType.OTP_WITHDRAWAL -> {
                    toolbarTitle.value = Constants.TITLE_OTP_WITHDRAW
                    settingsRepository.getAccountsOtp()?.let {
                        addAccountList(it)
                    }
                }
                CollectionType.OTP_DEPOSIT -> {
                    toolbarTitle.value = Constants.TITLE_OTP_DEPOSIT
                    settingsRepository.getAccountsOtp()?.let {
                        addAccountList(it)
                    }
                }
            }
            callback?.invoke()
        }
        isAccountTypeRequired.value = _collectionType == CollectionType.SINGLE
    }

    private fun addAccountList(it: List<NameValuePair>) {
        localAccountList.addAll(it)
        _accountList.value = localAccountList.map { it.name }
    }

    fun onSelectAccountClicked() {
        selectAccountClickEvent.call()
    }

    fun setAccountSearchResult(data: SearchAccountData?) {
        // get account details
        if (data != null)
            viewModelScope.launch {
                _isAccountDetailsLoading.value = true
                if (selectedAccountCode != null) {
                    when (val result =
                        accountRepository.getAccountInfo(selectedAccountCode!!, data.slug)) {
                        is Result.Success -> result.data.also {
                            accountInfo = it
                            slug = result.data.acSlug
                            hasAccountDetails.value = true
                        }
                        is Result.Error -> fetchAccountInfoFailureEvent.value = result.error
                    }
                }
                _isAccountDetailsLoading.value = false
            }
    }

    private fun notifyAccountTypeChanged() {
        memberName.value = ""
        memberNumber.value = ""
        memberScheme.value = ""
        accountNumber.value = ""
        openDate.value = ""
        amountBalance.value = ""
        amountToPay.value = ""
        slug = ""
        hasAccountDetails.value = false
    }

    fun onSubmitButtonClicked() {
        viewModelScope.launch {
            loading(true)
            when (_collectionType) {
                CollectionType.OTP_DEPOSIT -> {
                    cashDebitOrWithdrawalWithOtp(Constants.OTP_DEPOSIT_VALUE)
                }
                CollectionType.OTP_WITHDRAWAL -> {
                    cashDebitOrWithdrawalWithOtp(Constants.OTP_WITHDRAWAL_VALUE)
                }
                CollectionType.SINGLE -> {
                    simpleCashDeposit()
                }
            }

            loading(false)
        }
    }

    private suspend fun simpleCashDeposit() {
        if (selectedAccountCode != null) {
            when (val result =
                accountRepository.submitCollection(
                    selectedAccountCode!!,
                    slug,
                    amount.value ?: "0",
                    selectedPaymentMode.value ?: "",
                    utrNo.value ?: "",
                    transactionDate.value ?: "",
                    if(isNonCashPaymentSelected.value == true) _selectedBankAccountId ?: "" else "",
                    if(isOnlineTransferSelected.value == true) _selectedTransactionMode.value ?: "" else "",
                    if(isChequeSelected.value == true) _selectedBankName.value ?: "" else "",
                    remarks.value ?: ""
                )) {
                is Result.Success -> collectionSubmitSuccessEvent.value = result.data!!
                is Result.Error -> collectionSubmitFailureEvent.value = result.error
            }
        }
    }

    private suspend fun cashDebitOrWithdrawalWithOtp(collectionType: String) {
        if (selectedAccountCode != null) {
            val result =
                accountRepository.submitCollectionOtp(
                    selectedAccountCode!!,
                    slug,
                    amount.value ?: "0",
                    tType = collectionType
                )
            when (result) {
                is Result.Success -> _otpRequestResultData.postValue(result)
                is Result.Error -> collectionSubmitFailureEvent.value = result.error
            }
        }
    }

    fun resendOtp(otpId: String) {
        viewModelScope.launch {
            when (val result = accountRepository.resendOtp(otpId)) {
                is Result.Success -> _otpRequestResultData.postValue(result)
                is Result.Error -> collectionSubmitFailureEvent.value = result.error
            }
        }
    }

    fun verifyOtp(otpId: String, otpCode: String?) {
        viewModelScope.launch {
            when (val result = accountRepository.verifyOtp(otpId, otpCode)) {
                is Result.Success -> collectionSubmitSuccessEvent.value = result.data!!
                is Result.Error -> collectionSubmitFailureEvent.value = result.error
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        _isSubmitButtonEnabled.removeSource(accountType)
        _isSubmitButtonEnabled.removeSource(accountNumber)
        _isSubmitButtonEnabled.removeSource(amount)
        _isSubmitButtonEnabled.removeSource(selectedPaymentMode)
        _isSubmitButtonEnabled.removeSource(remarks)
    }
}