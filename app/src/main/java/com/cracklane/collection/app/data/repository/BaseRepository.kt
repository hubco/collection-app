package com.cracklane.collection.app.data.repository

import com.cracklane.collection.app.CollectionApp
import com.cracklane.collection.app.data.dataclass.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import java.util.*

abstract class BaseRepository {

    protected suspend fun <T> makeNetworkCall(
        networkCall: suspend () -> Response<T>
    ) = withContext(Dispatchers.IO) {
        try {
            val response = networkCall()
            if (response.isSuccessful)
                Result.Success(response.body()!!)
            else
                response.errorBody()?.let {
                    val errorMessage = getErrorMessage(it.string())
                    if (errorMessage.equals("unauthorized", ignoreCase = true)) {
                        notifyUnAuthorizedLogin()
                        Result.Unauthorized()
                    } else {
                        Result.Error(errorMessage)
                    }
                } ?: Result.Error(response.message())
        } catch (ex: Exception) {
            Result.Error(ex.localizedMessage ?: "Something went wrong, please try again!")
        }
    }

    private fun notifyUnAuthorizedLogin() {
        CollectionApp.notifySessionExpired()
    }

    private fun getErrorMessage(data: String): String {
        val genericError = "Something went wrong, please try again!"
        return try {
            val jsonObject = JSONObject(data)
            if (jsonObject.has("status")
                && (jsonObject.getString("status")
                    .lowercase(Locale.getDefault()) == "error"
                        || jsonObject.getString("status")
                    .lowercase(Locale.getDefault()) == "failed")
            )
                jsonObject.getString("message")
            else
                genericError
        } catch (ex: JSONException) {
            genericError
        }
    }
}