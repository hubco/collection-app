package com.cracklane.collection.app.common.architecture

import android.os.SystemClock
import android.view.View
import androidx.databinding.BindingAdapter

private const val SINGLE_CLICK_INTERVAL: Long = 400
private const val DOUBLE_CLICK_INTERVAL: Long = 600

abstract class SingleClickEvent : View.OnClickListener {
    private var lastClickTime: Long = 0

    abstract fun onSingleClick(v: View)

    override fun onClick(v: View) {
        val currentClickTime = SystemClock.uptimeMillis()
        val elapsedTime = currentClickTime - lastClickTime
        lastClickTime = currentClickTime
        if (elapsedTime <= SINGLE_CLICK_INTERVAL) return
        onSingleClick(v)
    }
}

abstract class DoubleClickEvent : View.OnClickListener {
    private var lastClickTime: Long = 0

    abstract fun onDoubleClick(v: View)

    override fun onClick(v: View) {
        val currentClickTime = SystemClock.uptimeMillis()
        val elapsedTime = currentClickTime - lastClickTime
        lastClickTime = currentClickTime
        if (elapsedTime <= DOUBLE_CLICK_INTERVAL)
            onDoubleClick(v)
    }
}

fun View.singleClickEvent(listener: (View) -> Unit) {
    setOnClickListener(object : SingleClickEvent() {
        override fun onSingleClick(v: View) {
            listener.invoke(v)
        }
    })
}

fun View.doubleClickEvent(listener: (View) -> Unit) {
    setOnClickListener(object : DoubleClickEvent() {
        override fun onDoubleClick(v: View) {
            listener.invoke(v)
        }
    })
}

// override onClick with single click event
@BindingAdapter("android:onClick")
fun setClickListener(view: View, listener: View.OnClickListener) {
    view.singleClickEvent {
        listener.onClick(it)
    }
}

// binding adapter to capture double click event
@BindingAdapter("android:onDoubleClick")
fun setDoubleClickListener(view: View, listener: View.OnClickListener) {
    view.doubleClickEvent {
        listener.onClick(it)
    }
}