package com.cracklane.collection.app.data.local.db.dao

import androidx.room.*
import com.cracklane.collection.app.data.remote.response.SettingsResponse

@Dao
interface AppDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSettings(settings: SettingsResponse)

    @Query("SELECT * from SettingsResponse")
    suspend fun getSettings(): SettingsResponse?

    @Transaction
    suspend fun saveSettings(settings: SettingsResponse) {
        deleteSettings()
        insertSettings(settings)
    }

    @Query("DELETE FROM SettingsResponse")
    suspend fun deleteSettings()
}