package com.cracklane.collection.app.ui.view.activity.search

import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.common.setupClearButtonWithAction
import com.cracklane.collection.app.data.remote.response.SearchAccountData
import com.cracklane.collection.app.databinding.ActivitySearchAccountBinding
import com.cracklane.collection.app.ui.navigation.setResult
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchAccountActivity : BaseActivity<ActivitySearchAccountBinding>() {

    override val viewModel: SearchAccountViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_search_account

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.etSearchAccount.setupClearButtonWithAction()
        binding.callback = object : SearchItemCallback {
            override fun onItemClicked(data: SearchAccountData) {
                setResult(data)
            }
        }
        viewModel.accountType = intent.getStringExtra(Constants.EXTRA_ACCOUNT_TYPE) ?: ""
    }

    override fun registerObserver() {}
}

@BindingAdapter("searchResults", "callback", requireAll = false)
fun RecyclerView.setSearchResults(
    result: List<SearchAccountData>?,
    callback: SearchItemCallback?
) {
    result?.let {
        addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayoutManager.VERTICAL
            )
        )
        layoutManager = LinearLayoutManager(context)
        adapter = SearchListAdapter(it) { data ->
            callback?.onItemClicked(data)
        }
    }
}

interface SearchItemCallback {
    fun onItemClicked(data: SearchAccountData)
}