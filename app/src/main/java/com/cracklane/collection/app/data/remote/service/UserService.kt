package com.cracklane.collection.app.data.remote.service

import com.cracklane.collection.app.data.remote.response.AgentInfoResponse
import retrofit2.Response
import retrofit2.http.GET

interface UserService {

    @GET("/api/agent/v1/agent-info")
    suspend fun getAccountInfo(): Response<AgentInfoResponse>

}