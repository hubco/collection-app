package com.cracklane.collection.app.ui.view.activity.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.common.session.ISession
import com.cracklane.collection.app.ui.navigation.navigateToLogin
import com.cracklane.collection.app.common.extensions.toast
import javax.inject.Inject

abstract class CoreActivity : AppCompatActivity() {

    @Inject
    lateinit var session: ISession

    protected fun setupToolBar(toolbar: Toolbar?, hasUpAction: Boolean) {
        toolbar?.let {
            setSupportActionBar(it)
            // set up button state
            supportActionBar?.run {
                title = null
                setDisplayHomeAsUpEnabled(hasUpAction)
                setDisplayShowHomeEnabled(hasUpAction)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(Constants.ACTION_SESSION_EXPIRED))
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            lifecycleScope.launchWhenResumed {
                toast("Session Expired!")
                session.run {
//                    clearSession()
                    logout()
                }
                navigateToLogin()
            }
        }
    }
}