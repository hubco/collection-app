package com.cracklane.collection.app.domain.repository

import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.request.OtpVerificationRequest
import com.cracklane.collection.app.data.remote.response.GenericResponse

interface IEnquiryRepository {

    suspend fun postEntry(
        inquiryType: String,
        firstName: String,
        lastName: String,
        aadharNo: String,
        panNo: String,
        address: String,
        mobNo: String,
        dob: String,
        remarks: String,
        photo: String?
    ): Result<OtpVerificationRequest>

    suspend fun verifyOtp(
        otpId: String,
        otpCode: String
    ): Result<GenericResponse>
}