package com.cracklane.collection.app.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.cracklane.collection.app.CollectionApp
import com.cracklane.collection.app.common.session.ISession
import com.cracklane.collection.app.common.session.SessionImpl
import com.cracklane.collection.app.data.local.db.AppDatabase
import com.cracklane.collection.app.data.local.db.DB_NAME
import com.cracklane.collection.app.data.local.db.dao.AppDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideApplication(app: Application): CollectionApp = app as CollectionApp

    @Provides
    fun provideContext(app: Application): Context {
        return app.applicationContext
    }

    @Provides
    fun provideSessionManager(context: Context): ISession = SessionImpl(context)

    @Provides
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun provideAppDao(db: AppDatabase): AppDao = db.appDao()

//    val MIGRATION_1_2 = object : Migration(1,2) {
//        override fun migrate(database: SupportSQLiteDatabase) {
//            database.execSQL("CREATE TABLE")
//        }
//    }
}