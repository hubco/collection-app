package com.cracklane.collection.app.ui.fragment.base.dialog

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cracklane.collection.app.data.remote.response.NameValuePair
import com.cracklane.collection.app.domain.repository.ISettingsRepository
import com.cracklane.collection.app.ui.view.activity.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.properties.Delegates

@HiltViewModel
class FilterViewModel @Inject constructor(private val settingsRepository: ISettingsRepository) :
    BaseViewModel() {

    val isTransactionListFilter = MutableLiveData(true)

    private var localAccountList = mutableListOf<NameValuePair>()
    private var _accountList = MutableLiveData<List<String>>()
    val accountList: LiveData<List<String>> = _accountList

    val accountType = MutableLiveData<String>()
    val accountCode: String
        get() = localAccountList.find { it.name == accountType.value }?.value ?: ""

    val fromDate = MutableLiveData<String>()
    val toDate = MutableLiveData<String>()

    private val calendar = Calendar.getInstance()
    var fromDay by Delegates.notNull<Int>()
    var fromMonth by Delegates.notNull<Int>()
    var fromYear by Delegates.notNull<Int>()
    var isFromDateChanged: Boolean = false

    var toDay by Delegates.notNull<Int>()
    var toMonth by Delegates.notNull<Int>()
    var toYear by Delegates.notNull<Int>()
    var isToDateChanged: Boolean = false

    var type: String = ""

    init {
        setupTodayDate()
        viewModelScope.launch {
            settingsRepository.getAccounts()?.let {
                addAccountList(it)
            }
        }
    }

    private fun addAccountList(it: List<NameValuePair>) {
        localAccountList.addAll(it)
        _accountList.value = localAccountList.map { it.name }
    }

    fun setupTodayDate() {
        fromDay = calendar.get(Calendar.DAY_OF_MONTH)
        fromMonth = calendar.get(Calendar.MONTH)
        fromYear = calendar.get(Calendar.YEAR)
        toDay = fromDay
        toMonth = fromMonth
        toYear = fromYear

        updateDates()
    }

    fun updateDates() {
        val delegateFromMonth = if(isFromDateChanged) fromMonth else fromMonth + 1
        val delegateToMonth = if(isToDateChanged) toMonth else toMonth + 1
        fromDate.value = "$fromDay/$delegateFromMonth/$fromYear"
        toDate.value = "$toDay/$delegateToMonth/$toYear"
    }

    fun setToDateAsFromDate() {
        toDay = fromDay
        toMonth = fromMonth
        toYear = fromYear
        isToDateChanged = true
        updateDates()
    }

    fun getToMinDate(): Long = Calendar.getInstance().apply {
        set(Calendar.DAY_OF_MONTH, fromDay)
        set(Calendar.MONTH, fromMonth - 1)
        set(Calendar.YEAR, fromYear)
    }.timeInMillis

}