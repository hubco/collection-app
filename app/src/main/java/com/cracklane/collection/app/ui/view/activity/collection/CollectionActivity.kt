package com.cracklane.collection.app.ui.view.activity.collection

import android.app.AlertDialog
import android.os.Bundle
import androidx.activity.viewModels
import com.cracklane.collection.app.R
import com.cracklane.collection.app.common.CollectionType
import com.cracklane.collection.app.common.Constants
import com.cracklane.collection.app.common.extensions.dialog.datePickerDialog
import com.cracklane.collection.app.common.extensions.dialog.errorAlert
import com.cracklane.collection.app.common.extensions.dialog.otpAlert
import com.cracklane.collection.app.common.extensions.dialog.printAlert
import com.cracklane.collection.app.common.put
import com.cracklane.collection.app.data.dataclass.Result
import com.cracklane.collection.app.data.remote.response.CollectionData
import com.cracklane.collection.app.data.remote.response.SearchAccountData
import com.cracklane.collection.app.data.remote.response.toAccountInfo
import com.cracklane.collection.app.databinding.ActivityCollectionBinding
import com.cracklane.collection.app.ui.navigation.Routes
import com.cracklane.collection.app.ui.navigation.navigateForResult
import com.cracklane.collection.app.ui.navigation.navigateTo
import com.cracklane.collection.app.ui.view.activity.base.BaseActivity
import com.cracklane.collection.app.ui.view.activity.search.SearchAccountActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CollectionActivity : BaseActivity<ActivityCollectionBinding>() {

    override val viewModel: CollectionViewModel by viewModels()
    override val layoutRes: Int = R.layout.activity_collection

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true
    private var otpAlertDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.tvAccountType.setOnClickListener {
            binding.spnAccountType.performClick()
        }

        binding.tvTransactionDate.setOnClickListener {
            datePickerDialog {
                viewModel.transactionDate.value = it
            }
        }

        // check if we have collection data
        intent.getParcelableExtra<CollectionData>(Constants.EXTRA_COLLECTION_DATA)?.let {
            viewModel.setCollectionType(CollectionType.SINGLE) {
                binding.tvAccountType.isEnabled = false
                binding.tvAccountNumber.isEnabled = false
                viewModel.hasCollectionData.value = true
                viewModel.selectedAccountCode = it.acType
                viewModel.accountInfo = it.toAccountInfo()
//                viewModel.accountNumber.value = it.acNo
//                viewModel.memberName.value = it.memberName
//                viewModel.memberNumber.value = it.memberNo
//                viewModel.memberScheme.value = it.scheme
//                viewModel.openDate.value = it.openDate
//                viewModel.amountBalance.value = "${it.acBalance}"
//                viewModel.amountToPay.value = "${it.amountToCollect}"
//                viewModel.slug = it.acSlug
//                viewModel.hasAccountDetails.value = true
            }
        } ?: run {
            // check collection type
            val collectionType = CollectionType.values()[intent.getIntExtra(
                Constants.EXTRA_COLLECTION_TYPE,
                CollectionType.SINGLE.ordinal,
            )]
            viewModel.setCollectionType(collectionType)
        }
    }

    override fun registerObserver() {
        super.registerObserver()
        viewModel.selectAccountClickEvent.observe(this) {
            viewModel.selectedAccountCode.let { code ->
                navigateForResult<SearchAccountActivity, SearchAccountData>(Bundle().apply {
                    put(Constants.EXTRA_ACCOUNT_TYPE, code)
                }) { success, data ->
                    if (success) {
                        viewModel.setAccountSearchResult(data)
                    }
                }
            }
        }

        viewModel.fetchAccountInfoFailureEvent.observe(this) {
            errorAlert(it)
        }

        viewModel.collectionSubmitSuccessEvent.observe(this) {
            hideProgressDialog()
            otpAlertDialog?.dismiss()
            printAlert("Collection submitted!\nRef id: ${it.transactionId}", printCallback = {
                navigateTo(
                    Routes.PRINT_COLLECTION_SCREEN,
                    mutableMapOf<String, Any?>().apply {
                        put(Constants.EXTRA_COLLECTION_RESPONSE, it)
                        put(Constants.EXTRA_ACCOUNT_INFO, viewModel.accountInfo)
                        put("amount", viewModel.amount.value)
                        put("remarks", viewModel.remarks.value)
                    })
                finish()
            }, okCallback = {
                finish()
            })
        }

        viewModel.collectionSubmitFailureEvent.observe(this) {
            hideProgressDialog()
            errorAlert(it)
        }
        viewModel.otpRequestResultData.observe(this) {
            hideProgressDialog()
            when (it) {
                is Result.Success -> {
                    otpAlertDialog =
                        otpAlert(it.data.message, "") { isResendOtp, otpCode ->
                            when (isResendOtp) {
                                true -> {
                                    otpAlertDialog?.dismiss()
                                    showProgressDialog()
                                    viewModel.resendOtp(it.data.otpId)
                                }
                                false -> {
                                    showProgressDialog()
                                    viewModel.verifyOtp(it.data.otpId, otpCode)
                                }
                            }
                        }
                }
                is Result.Error -> {
                    hideProgressDialog()
                    otpAlertDialog?.dismiss()
                    errorAlert(it.error)
                }
            }
        }

    }
}