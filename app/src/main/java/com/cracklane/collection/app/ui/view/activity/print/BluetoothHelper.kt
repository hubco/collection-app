package com.cracklane.collection.app.ui.view.activity.print

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import javax.inject.Inject

sealed class BluetoothStatus {
    object Scanning : BluetoothStatus()
    class Finished(deviceName: String? = null) : BluetoothStatus()
}

class BluetoothHelper @Inject constructor(private val context: Context) : DefaultLifecycleObserver {

    private val btAdapter =
        (context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter

    private var callbackDelegate: ((status: BluetoothStatus) -> Unit)? = null

    private val btReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                BluetoothAdapter.ACTION_DISCOVERY_STARTED -> callbackDelegate?.let {
                    it(
                        BluetoothStatus.Scanning
                    )
                }
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> callbackDelegate?.let {
                    it(
                        BluetoothStatus.Finished()
                    )
                }
                BluetoothDevice.ACTION_FOUND -> callbackDelegate?.let {
                    it(
                        BluetoothStatus.Finished(
                            intent.getParcelableExtra<BluetoothDevice>(
                                BluetoothDevice.EXTRA_DEVICE
                            )!!.name
                        )
                    )
                }
            }
        }
    }

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        val intentFilter = IntentFilter().apply {
            addAction(BluetoothDevice.ACTION_FOUND)
            addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED)
            addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        }
        context.registerReceiver(btReceiver, intentFilter)
    }

    fun scanDevices(callback: (status: BluetoothStatus) -> Unit) {
        callbackDelegate = callback
        btAdapter.startDiscovery()
    }

    override fun onPause(owner: LifecycleOwner) {
        super.onPause(owner)
        if (btAdapter.isDiscovering) btAdapter.cancelDiscovery()
    }

    override fun onDestroy(owner: LifecycleOwner) {
        context.unregisterReceiver(btReceiver)
    }
}